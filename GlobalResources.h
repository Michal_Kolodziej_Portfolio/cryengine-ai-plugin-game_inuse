#pragma once
#include "StdAfx.h"
#include "CryRenderer/IRenderAuxGeom.h"

Vec3 RotatePoint(const Vec3 origin, const Vec3 pointToRotate, const float angle)
{
	const float sinus = sin(angle);
	const float cosinus = cos(angle);

	Vec3 returnPoint = pointToRotate - origin;

	float xNew = returnPoint.x * cosinus - returnPoint.y * sinus;
	float yNew = returnPoint.x * sinus + returnPoint.y * cosinus;

	returnPoint.x = xNew + origin.x;
	returnPoint.y = yNew + origin.y;

	returnPoint.z = gEnv->p3DEngine->GetTerrainElevation(returnPoint.x, returnPoint.y);

	return returnPoint;
}

Vec3 CorrectNavigationPoint(NavigationAgentTypeID agentId, const Vec3 inputPosition)
{
	Vec3 correctedVector;
	gEnv->pAISystem->GetNavigationSystem()->GetClosestPointInNavigationMesh(agentId, inputPosition, 100.f, 100.f, &correctedVector);
	return correctedVector;
}


void DrawCircleOnTerrain(const Vec3 center, const float radius, const ColorF color, const float thickness, Vec3 *ret, bool correctToNavigation = false, NavigationAgentTypeID agentID = (NavigationAgentTypeID)0)
{
	const float M_PI = 3.14159265359f;
	const float angle = 15.f;
	const float rad = (angle * M_PI) / 180.f;
	const int polyPts = 24;
	Vec3 polyPoints[polyPts];
	polyPoints[0] = center + Vec3(radius, 0.f, 0.f);
	polyPoints[0].z = gEnv->p3DEngine->GetTerrainElevation(polyPoints[0].x, polyPoints[0].y);
	if (correctToNavigation)
		polyPoints[0] = CorrectNavigationPoint(agentID, polyPoints[0]);
	ret[0] = polyPoints[0];

	for (int i = 1; i < polyPts; i++)
	{
		polyPoints[i] = RotatePoint(center, polyPoints[i - 1], rad);
		if (correctToNavigation)
			polyPoints[i] = CorrectNavigationPoint(agentID, polyPoints[i]);
		ret[i] = polyPoints[i];
	}
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawPolyline(polyPoints, polyPts, true, color, thickness);
}

NavigationMeshID CreateNavigationMesh(Vec3 *vertices, const int verticesCount, const float height, const Vec3 tileSize, const int tileCount, const string agentName, const string meshName, const bool exclusion = false)
{
	INavigationSystem *pNavSystem = gEnv->pAISystem->GetNavigationSystem();

	NavigationVolumeID volumeID = pNavSystem->CreateVolume(vertices, verticesCount, height);
	INavigationSystem::CreateMeshParams para;
	para.origin = vertices[0];
	para.tileSize = tileSize;
	para.tileCount = tileCount;
	NavigationAgentTypeID agentID = pNavSystem->GetAgentTypeID(agentName);
	NavigationMeshID meshID = pNavSystem->CreateMeshForVolumeAndUpdate(meshName, agentID, para, volumeID);
	if(exclusion)
		pNavSystem->SetExclusionVolume(&agentID, 1, volumeID);

	pNavSystem->SetMeshBoundaryVolume(meshID, volumeID);
	pNavSystem->RegisterArea(meshName, volumeID);

	return meshID;
}

enum class EResourceTypes
{
	RES_Gold_Ore = 0,
	RES_Gold,
	RES_ALL
};
static void ReflectType(Schematyc::CTypeDesc<EResourceTypes>& desc)
{
	desc.SetGUID("{B95D77D4-3DD1-4E68-9342-89BEB894731A}"_cry_guid);
	desc.SetLabel("Resource Type");
	desc.SetDescription("Resource type");
	desc.AddConstant(EResourceTypes::RES_Gold_Ore, "GoldOre", "Gold ore");
	desc.AddConstant(EResourceTypes::RES_Gold, "Gold", "Gold");
}

string RESOURCE_NAME[2] = { "Gold Ore", "Gold" };