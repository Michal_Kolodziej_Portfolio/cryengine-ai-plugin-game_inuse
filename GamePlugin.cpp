#include "StdAfx.h"
#include "GamePlugin.h"

#include "Components/Player.h"

#include <IGameObjectSystem.h>
#include <IGameObject.h>

#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/EnvPackage.h>
#include <CrySchematyc/Utils/SharedString.h>

// Included only once per DLL module.
#include <CryCore/Platform/platform_impl.inl>
#include "Components/LandOwner.h"

#include <atlbase.h>
#include <sstream>

std::vector<CResourceAreaComponent*> CGamePlugin::resourceAreas;
std::vector<CLandOwnerComponent*> CGamePlugin::landOwners;
std::vector<CGamePlugin::UnitPublicProperties*> CGamePlugin::unitPublicProperties;

CGamePlugin::~CGamePlugin()
{
	// Remove any registered listeners before 'this' becomes invalid
	gEnv->pGameFramework->RemoveNetworkedClientListener(*this);
	gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);

	if (gEnv->pSchematyc)
	{
		gEnv->pSchematyc->GetEnvRegistry().DeregisterPackage(CGamePlugin::GetCID());
	}
}

CGamePlugin::CGamePlugin()
{
	RegisterFlowNodes();
	RegisterCVars();
}

bool CGamePlugin::Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams)
{
	// Register for engine system events, in our case we need ESYSTEM_EVENT_GAME_POST_INIT to load the map
	gEnv->pSystem->GetISystemEventDispatcher()->RegisterListener(this, "CGamePlugin");
	// Listen for client connection events, in order to create the local player
	gEnv->pGameFramework->AddNetworkedClientListener(*this);

	//initialize unit properties
	XmlNodeRef unitPropsXml = gEnv->pSystem->LoadXmlFromFile("SchematycEntities/UnitsProperties.xml");
	if (unitPropsXml)
	{
		for (int i = 0; i < unitPropsXml->getChildCount(); i++)
		{
			if (XmlNodeRef unt = unitPropsXml->getChild(i))
			{
				unitPublicProperties.push_back(new UnitPublicProperties(unt->getAttr("name"), atoi(unt->getAttr("buildCost")), atoi(unt->getAttr("buildTime"))));
			}
		}
	}

	return true;
}

void CGamePlugin::OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam)
{
	switch (event)
	{
	case ESYSTEM_EVENT_REGISTER_SCHEMATYC_ENV:
	{
		// Register all components that belong to this plug-in
		auto staticAutoRegisterLambda = [](Schematyc::IEnvRegistrar& registrar)
		{
			// Call all static callback registered with the CRY_STATIC_AUTO_REGISTER_WITH_PARAM
			Detail::CStaticAutoRegistrar<Schematyc::IEnvRegistrar&>::InvokeStaticCallbacks(registrar);
		};

		if (gEnv->pSchematyc)
		{
			gEnv->pSchematyc->GetEnvRegistry().RegisterPackage(
				stl::make_unique<Schematyc::CEnvPackage>(
					CGamePlugin::GetCID(),
					"EntityComponents",
					"Crytek GmbH",
					"Components",
					staticAutoRegisterLambda
					)
			);
		}
	}
	break;
		// Called when the game framework has initialized and we are ready for game logic to start
	case ESYSTEM_EVENT_GAME_POST_INIT:
	{
		// Don't need to load the map in editor
		if (!gEnv->IsEditor())
		{
			gEnv->pConsole->ExecuteString("map ft_showcase", false, true);
		}
	}
	break;
	}
}

bool CGamePlugin::RegisterFlowNodes()
{
	CryRegisterFlowNodes();
	return true;
}

bool CGamePlugin::UnregisterFlowNodes()
{
	CryUnregisterFlowNodes();
	return true;
}

void CGamePlugin::RegisterCVars()
{
	REGISTER_COMMAND("strategy_create", (ConsoleCommandFunc)CPlayerComponent::CVarHandler, VF_NULL, "Creating unit");
}

CLandOwnerComponent * CGamePlugin::GetLandOwnerByTeam(int tm)
{
	for each(CLandOwnerComponent *lo in landOwners)
	{
		if (lo->GetTeam() == tm)
		{
			return lo;
		}
	}
	return nullptr;
}


bool CGamePlugin::OnClientConnectionReceived(int channelId, bool bIsReset)
{
	return true;
}

bool CGamePlugin::OnClientReadyForGameplay(int channelId, bool bIsReset)
{
	return true;
}

void CGamePlugin::OnClientDisconnected(int channelId, EDisconnectionCause cause, const char* description, bool bKeepClient)
{
}

CRYREGISTER_SINGLETON_CLASS(CGamePlugin)