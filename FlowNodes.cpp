#include "StdAfx.h"
#include "CryFlowGraph/IFlowBaseNode.h"
#include "CryFlowGraph/IFlowGraphModuleManager.h"
#include "Components/AICharacterComponent.h"
#include "CryMath/Random.h"
#include "Components/Player.h"
#include "GamePlugin.h"
#include <CryAISystem/NavigationSystem/MNMNavMesh.h>
#include "Components/CitizenComponent.h"
#include "Components/LandOwner.h"
#include "Components/BuildingComponent.h"
#include "CryAISystem/INavigation.h"
#include "Components/StorageBuilding.h"
#include "Components/ResourceArea.h"
#include "CryAISystem/IPathfinder.h"

Vec3 RandomizeVectorInRadius(const Vec3 vecToRandomize, const float radius)
{
	const float halfRad = radius / 2.f;
	const float x = vecToRandomize.x;
	const float y = vecToRandomize.y;
	const float x_rand = cry_random(x - halfRad, x + halfRad);
	const float y_rand = cry_random(y - halfRad, y + halfRad);
	const Vec3 newVec = Vec3(x_rand, y_rand, vecToRandomize.z);
	return newVec;
}
float GetDistanceBetweenEntities(IEntity *pEntity1, IEntity *pEntity2)
{
	if (pEntity1 && pEntity2)
	{
		const Vec3 first = pEntity1->GetWorldPos();
		const Vec3 second = pEntity2->GetWorldPos();
		const Vec3 diff = second - first;
		const float flat_distance = sqrt(powf(diff.x, 2.f) + powf(diff.y, 2.f));
		const float threeDee_distance = sqrt(powf(diff.y, 2.f) + powf(diff.z, 2.f));
		return threeDee_distance < 0.f ? threeDee_distance * (-1) : threeDee_distance;
	}
	return -1.f;
}
float GetDistanceBetweenVectors(const Vec3 vector1, const Vec3 vector2)
{
	const Vec3 diff = vector2 - vector1;
	const float flat_distance = sqrt(powf(diff.x, 2.f) + powf(diff.y, 2.f));
	const float threeDee_distance = sqrt(powf(diff.y, 2.f) + powf(diff.z, 2.f));
	return threeDee_distance < 0.f ? threeDee_distance * (-1) : threeDee_distance;
}
Vec3 CorrectNavigationPt(NavigationAgentTypeID agentId, const Vec3 inputPosition)
{
	Vec3 correctedVector;
	gEnv->pAISystem->GetNavigationSystem()->GetClosestPointInNavigationMesh(agentId, inputPosition, 100.f, 100.f, &correctedVector);
	return correctedVector;
}

//AIFury nodes
class CAIFury_GetRandomNavigablePointInRadius : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_VECTOR,
		IN_RADIUS
	};
	enum Outputs
	{
		OUT_FAILED = 0,
		OUT_VECTOR
	};
public:
	CAIFury_GetRandomNavigablePointInRadius(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Randomize", _HELP("Randomize vector around radius")),
			InputPortConfig<Vec3>("Vector", _HELP("Vector to randomize")),
			InputPortConfig<float>("Radius", _HELP("Radius around vector to randomize")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output trigger")),
			OutputPortConfig<Vec3>("OutputVector", _HELP("Vector after randomizing")),
			{ 0 }
		};

		config.sDescription = "Randomizes vector around radius";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SPathfindingComponent *pPathfinder = pOwnerEntity->GetComponent<SPathfindingComponent>())
				{

					TFlowInputData *inputVector = pActInfo->GetInputPort(IN_VECTOR);
					Vec3 *inputVectorValue = inputVector->GetPtr<Vec3>();
					TFlowInputData *inputRadius = pActInfo->GetInputPort(IN_RADIUS);
					float *inputRadiusValue = inputRadius->GetPtr<float>();
					if (inputVectorValue && inputRadiusValue)
					{
						Vec3 vector = *inputVectorValue;
						const float radius = *inputRadiusValue;
						for (int i = 0; i < 10000; i++)
						{
							//randomize input vector
							vector = RandomizeVectorInRadius(vector, radius);
							const Vec3 corr = pPathfinder->GetClosestValidNavigableLocation(vector);
							//check if point is navigable
							const bool bNavigable = pPathfinder->IsLocationPossibleToReach(vector);
							//CryLogAlways("BEFORE CORECTION = %f %f %f", vector.x, vector.y, vector.z);
							//CryLogAlways("AFTER CORECTION = %f %f %f", corr.x, corr.y, corr.z);
							//if point is navigable, then stop randomizing and trigger outputs
							if (bNavigable)
							{
								ActivateOutput(pActInfo, OUT_VECTOR, vector);
								return;
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FAILED, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_GetRandomNavigablePointInRadius(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_AI_WalkTo : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_LOCATION
	};
	enum Outputs
	{
		OUT_TRIGGER = 0
	};
public:
	CAIFury_AI_WalkTo(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Start", _HELP("Start walking to")),
			InputPortConfig<Vec3>("Location", _HELP("TargetLocation")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Succeeded", _HELP("Output trigger")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				TFlowInputData *targetLocation = pActInfo->GetInputPort(IN_LOCATION);
				Vec3 *targetLocationValue = targetLocation->GetPtr<Vec3>();
				if (targetLocationValue)
				{
					const Vec3 vector = *targetLocationValue;
					if (SPathfindingComponent *pPathfinder = pOwnerEntity->GetComponent<SPathfindingComponent>())
					{
						if (pPathfinder->IsProcessingRequest())
							pPathfinder->CancelCurrentRequest();

						pPathfinder->RequestMoveTo(vector);
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_AI_WalkTo(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_IsMoving : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_CHECK = 0
	};
	enum Outputs
	{
		OUT_ISMOVING = 0
	};
public:
	CAIFury_IsMoving(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Calculate", _HELP("Calculate valid position")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<bool>("IsMoving", _HELP("Check if ai is moving")),
			{ 0 }
		};

		config.sDescription = "Return true or false value of this module";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_CHECK))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SPathfindingComponent *pPathfinder = pEntity->GetComponent<SPathfindingComponent>())
				{
					const bool bIsMoving = pPathfinder->IsMoving();
					const bool bIsProcessingRequest = pPathfinder->IsProcessingRequest();
					ActivateOutput(pActInfo, OUT_ISMOVING, bIsMoving /*|| bIsProcessingRequest*/);
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_IsMoving(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_StopMoving : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_CHECK = 0
	};
	enum Outputs
	{
		OUT_MOVEMENT_STOPPED = 0
	};
public:
	CAIFury_StopMoving(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Stop", _HELP("Calculate valid position")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnStopped", _HELP("Check if ai is moving")),
			{ 0 }
		};

		config.sDescription = "Return true or false value of this module";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_CHECK))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SPathfindingComponent *pPathfinder = pEntity->GetComponent<SPathfindingComponent>())
				{
					pPathfinder->CancelCurrentRequest();
					ActivateOutput(pActInfo, OUT_MOVEMENT_STOPPED, 0);
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_StopMoving(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LookDirection : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_DIRECTION
	};
	enum Outputs
	{
		OUT_TRIGGER = 0
	};
public:
	CAIFury_LookDirection(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Look", _HELP("Start walking to")),
			InputPortConfig<Vec3>("LookDirection", _HELP("Target direction")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnLook", _HELP("Output trigger")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				TFlowInputData *targetDirection = pActInfo->GetInputPort(IN_DIRECTION);
				Vec3 *targetDirectionValue = targetDirection->GetPtr<Vec3>();
				if (targetDirectionValue)
				{
					const Vec3 direction = *targetDirectionValue;
					if (CAICharacterComponent *pai = pOwnerEntity->GetComponent<CAICharacterComponent>())
					{
						//if (IsPortActive(pActInfo, IN_DIRECTION))
						pai->lookDir = direction;
						//else
						//	pai->lookDir = ZERO;
					}

					ActivateOutput(pActInfo, OUT_TRIGGER, 0);
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LookDirection(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_Fire : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_ONFIRED = 0
	};
public:
	CAIFury_Fire(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Fire", _HELP("Start walking to")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnFired", _HELP("Player seen")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAICharacterComponent *pai = pOwnerEntity->GetComponent<CAICharacterComponent>())
				{
					pai->StartFire();
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_Fire(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_Wait : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_WAIT_TIME
	};
	enum Outputs
	{
		OUT_TRIGGER = 0,
	};
public:
	CAIFury_Wait(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("StartWaiting", _HELP("Start walking to")),
			InputPortConfig<int>("TimeInMiliseconds", _HELP("Owner AI")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnWait", _HELP("Player seen")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));

				TFlowInputData *waitTime = pActInfo->GetInputPort(IN_WAIT_TIME);
				int *waitTimeValue = waitTime->GetPtr<int>();

				if (waitTimeValue)
				{
					if (CAICharacterComponent *pai = pOwnerEntity->GetComponent<CAICharacterComponent>())
					{
						const int waitTimeMs = *waitTimeValue;
						pai->bIsWaiting = true;
						pai->iWaitTime = waitTimeMs;
						ActivateOutput(pActInfo, OUT_TRIGGER, 0);
						pOwnerEntity->SetTimer(100, waitTimeMs);
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_Wait(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_StopWaiting : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_TRIGGER = 0,
	};
public:
	CAIFury_StopWaiting(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("StartWaiting", _HELP("Start walking to")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnStop", _HELP("Player seen")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAICharacterComponent *pai = pEntity->GetComponent<CAICharacterComponent>())
				{
					pEntity->KillTimer(100);
					pai->bIsWaiting = false;
					pai->iWaitTime = 0;
					ActivateOutput(pActInfo, OUT_TRIGGER, 0);
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_StopWaiting(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_IsWaiting : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_RESULT = 0,
	};
public:
	CAIFury_IsWaiting(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("StartWaiting", _HELP("Start walking to")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<bool>("IsWaiting", _HELP("Player seen")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CAICharacterComponent *pai = pOwnerEntity->GetComponent<CAICharacterComponent>())
				{
					const bool isWaiting = pai->bIsWaiting;
					ActivateOutput(pActInfo, OUT_RESULT, isWaiting);
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_IsWaiting(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_CheckNavigationPoint : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_VECTOR
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_CheckNavigationPoint(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Check if point is navigable")),
			InputPortConfig<Vec3>("Point", _HELP("Point to check")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Navigable", _HELP("Triggered when the point was found to be navigable")),
			OutputPortConfig<SFlowSystemVoid>("NotNavigable", _HELP("Triggered when the point was found to be non-navigable")),
			{ 0 }
		};

		config.sDescription = "Checks if AI has ammo for specified weapon";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				//check if has weapon in inventory
				if (CAICharacterComponent *pai = pOwnerEntity->GetComponent<CAICharacterComponent>())
				{
					TFlowInputData *indata = pActInfo->GetInputPort(IN_VECTOR);
					if (indata)
					{
						Vec3 *vector = indata->GetPtr<Vec3>();
						if (SPathfindingComponent *pPathfinder = pOwnerEntity->GetComponent<SPathfindingComponent>())
						{
							const bool bNavigable = pPathfinder->IsLocationPossibleToReach(*vector);

							if (bNavigable)
								ActivateOutput(pActInfo, OUT_TRUE, 0);
							else
								ActivateOutput(pActInfo, OUT_FALSE, 0);
						}
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_CheckNavigationPoint(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_CanSeeEntity : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID,
		IN_RANGE
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_CanSeeEntity(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Trigger check")),
			InputPortConfig<EntityId>("EntityId", _HELP("EntityId to check if can see")),
			InputPortConfig<float>("Range", _HELP("Range of sight")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("True", _HELP("Entity seen")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Entity not seen")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				float fSightDist = 0.f;
				TFlowInputData *rangeData = pActInfo->GetInputPort(IN_RANGE);
				if (rangeData)
					fSightDist = *rangeData->GetPtr<float>();
				TFlowInputData *idData = pActInfo->GetInputPort(IN_ENTITYID);
				if (idData)
				{
					EntityId entId = *idData->GetPtr<EntityId>();
					if (IEntity *pEntity = gEnv->pEntitySystem->GetEntity(entId))
					{
						if (pEntity != pOwnerEntity)
						{
							Vec3 rayOrigin = pOwnerEntity->GetWorldPos() + Vec3(0.f, 0.f, 1.2f);
							Vec3 otherEntityPos = pEntity->GetWorldPos() + Vec3(0.f, 0.f, 1.2f);
							Vec3 dir = otherEntityPos - rayOrigin;
							ray_hit hit;
							static const unsigned int rayflags = rwi_stop_at_pierceable | rwi_colltype_any;
							if (gEnv->pPhysicalWorld->RayWorldIntersection(rayOrigin, dir, ent_all, rayflags, &hit, 1, pOwnerEntity->GetPhysicalEntity()))
							{
								if (hit.pCollider)
								{
									IEntity *pCollider = gEnv->pEntitySystem->GetEntityFromPhysics(hit.pCollider);
									if (pCollider && pCollider == pEntity)
									{
										if (hit.dist <= fSightDist)
										{
											ActivateOutput(pActInfo, OUT_TRUE, 0);
											return;
										}
									}
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_CanSeeEntity(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_GetClosestSeenEnemy : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_RANGE
	};
	enum Outputs
	{
		OUT_ENTITYID = 0,
		OUT_FALSE
	};
public:
	CAIFury_GetClosestSeenEnemy(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Trigger check")),
			InputPortConfig<float>("Range", _HELP("Range of sight")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("EnemyId", _HELP("Entity seen")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Entity not seen")),
			{ 0 }
		};

		config.sDescription = "Check if AI can see some enemy";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if(SCitizenComponent *pOwnerCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					float fSightDist = 20.f;
					TFlowInputData *rangeData = pActInfo->GetInputPort(IN_RANGE);
					if (rangeData)
						fSightDist = *rangeData->GetPtr<float>();

					EntityId playerInSightId = 0;
					Vec3 origin = pOwnerEntity->GetWorldPos();
					IPhysicalEntity **pEntityList = NULL;
					float last_dist = 1000000.f;
					int iEntityCount = gEnv->pEntitySystem->GetPhysicalEntitiesInBox(origin, fSightDist, pEntityList);
					for (int i = 0; i < iEntityCount; i++)
					{
						if (pEntityList[i])
						{
							if (IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pEntityList[i]))
							{
								if (pEntity != pOwnerEntity)
								{
									if (SCitizenComponent *pEnemyCitizen = pEntity->GetComponent<SCitizenComponent>())
									{
										if (pEnemyCitizen->GetTeam() != pOwnerCitizen->GetTeam())
										{
											float curDist = pOwnerEntity->GetWorldPos().GetDistance(pEntity->GetWorldPos());
											if (curDist < last_dist)
											{
												last_dist = curDist;
												Vec3 rayOrigin = pOwnerEntity->GetWorldPos() + Vec3(0.f, 0.f, 1.2f);
												Vec3 playerPos = pEntity->GetWorldPos() + Vec3(0.f, 0.f, 1.2f);
												Vec3 dir = playerPos - rayOrigin;
												ray_hit hit;
												static const unsigned int rayflags = rwi_stop_at_pierceable | rwi_colltype_any;
												if (gEnv->pPhysicalWorld->RayWorldIntersection(rayOrigin, dir, ent_all, rayflags, &hit, 1, pOwnerEntity->GetPhysicalEntity()))
												{
													if (hit.pCollider)
													{
														IEntity *pCollider = gEnv->pEntitySystem->GetEntityFromPhysics(hit.pCollider);
														if (pCollider && pCollider == pEntity)
														{
															ActivateOutput(pActInfo, OUT_ENTITYID, pEntity->GetId());
															return;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_GetClosestSeenEnemy(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_ManageAITimer : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_SET = 0,
		IN_GET, 
		IN_TIMERID,
		IN_MS
	};
	enum Outputs
	{
		OUT_RESULT = 0,
		OUT_FAILED
	};
public:
	CAIFury_ManageAITimer(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Timer set")),
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Timer get")),
			InputPortConfig<int>("TimerId", _HELP("Timer id")),
			InputPortConfig<int>("TimeInMiliseconds", _HELP("Timer time in miliseconds")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<bool>("TimerState", _HELP("Returns whether timer is set or not")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Fails when something goes wrong")),
			{ 0 }
		};

		config.sDescription = "Forces AI to walk to specified location";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{				
			IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
			if (CAICharacterComponent *pAI = pOwnerEntity->GetComponent<CAICharacterComponent>())
			{
				int *timerId = nullptr;
				int *ms = nullptr;

				if (TFlowInputData *idData = pActInfo->GetInputPort(IN_TIMERID))
					timerId = idData->GetPtr<int>();
				if (TFlowInputData *msData = pActInfo->GetInputPort(IN_MS))
					ms = msData->GetPtr<int>();

				//if we want to set timer
				if (IsPortActive(pActInfo, IN_SET))
				{
					if (timerId && ms)
					{
						if (*ms > 0)
						{
							pAI->Kill_Timer(*timerId);
							pAI->Set_Timer(*timerId, *ms);
							ActivateOutput(pActInfo, OUT_RESULT, true);
						}
						else
						{
							pAI->Kill_Timer(*timerId);
							ActivateOutput(pActInfo, OUT_RESULT, false);
						}
						return;
					}
				}
				//if we want to get timer state
				if (IsPortActive(pActInfo, IN_GET))
				{
					if (timerId)
					{
						if(pAI->TimerExists(*timerId))
						{
							ActivateOutput(pActInfo, OUT_RESULT, pAI->Get_TimerState(*timerId));
							return;
						}
					}
				}
			}
			ActivateOutput(pActInfo, OUT_FAILED, 0);
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_ManageAITimer(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_IsPointNavigable : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_VECTOR
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_IsPointNavigable(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Randomize vector around radius")),
			InputPortConfig<Vec3>("Vector", _HELP("Vector to randomize")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("True", _HELP("Output trigger")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Output trigger")),
			{ 0 }
		};

		config.sDescription = "Randomizes vector around radius";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SPathfindingComponent *pPathfinder = pOwnerEntity->GetComponent<SPathfindingComponent>())
				{

					TFlowInputData *inputVector = pActInfo->GetInputPort(IN_VECTOR);
					Vec3 *inputVectorValue = inputVector->GetPtr<Vec3>();
					if (inputVectorValue)
					{
						Vec3 vector = *inputVectorValue;
						const bool bNavigable = pPathfinder->IsLocationPossibleToReach(vector);
						if (bNavigable)
						{
							ActivateOutput(pActInfo, OUT_TRUE, 0);
							return;
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_IsPointNavigable(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_HasLocationChanged : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_LOCATION
	};
	enum Outputs
	{
		OUT_TRUE = 0, 
		OUT_FALSE
	};
public:
	CAIFury_HasLocationChanged(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Start", _HELP("Start walking to")),
			InputPortConfig<Vec3>("Location", _HELP("TargetLocation")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("True", _HELP("Output if true")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Output if false")),
			{ 0 }
		};

		config.sDescription = "Checks whether the old and new locations are different";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				TFlowInputData *targetLocation = pActInfo->GetInputPort(IN_LOCATION);
				Vec3 *targetLocationValue = targetLocation->GetPtr<Vec3>();
				if (targetLocationValue)
				{
					const Vec3 vector = *targetLocationValue;
					if (SPathfindingComponent *pPathfinder = pOwnerEntity->GetComponent<SPathfindingComponent>())
					{
						if (pPathfinder->GetCurrentRequestPosition() == vector)
							ActivateOutput(pActInfo, OUT_FALSE, 0);
						else
							ActivateOutput(pActInfo, OUT_TRUE, 0);
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_HasLocationChanged(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_GetClosestNavigablePoint : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_VECTOR
	};
	enum Outputs
	{
		OUT_CORRECTED_POINT = 0
	};
public:
	CAIFury_GetClosestNavigablePoint(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Gets closest navigable point")),
			InputPortConfig<Vec3>("Vector", _HELP("Vector to correct")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<Vec3>("CorrectedVector", _HELP("Output vector")),
			{ 0 }
		};

		config.sDescription = "Gets closest navigable point";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				Vec3 returnVector = ZERO;
				TFlowInputData *inputVector = pActInfo->GetInputPort(IN_VECTOR);
				if (inputVector)
				{
					NavigationAgentTypeID agentID = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
					returnVector = *inputVector->GetPtr<Vec3>();
					gEnv->pAISystem->GetNavigationSystem()->GetClosestPointInNavigationMesh(agentID, returnVector, 100.f, 100.f, &returnVector);
					gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(returnVector, 1.f, Col_Cyan);
				}
				ActivateOutput(pActInfo, OUT_CORRECTED_POINT, returnVector);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_GetClosestNavigablePoint(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_SupplierTakerReservation : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_BUILDINGID, 
		IN_ENTITYID
	};
	enum Outputs
	{
		OUT_TRIGGER = 0
	};
public:
	CAIFury_SupplierTakerReservation(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Set supplier taker")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Entity id of the building to set")),
			InputPortConfig<EntityId>("TakerId", _HELP("Entity id of the taker, 0 to reset")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnSet", _HELP("Output set")),
			{ 0 }
		};

		config.sDescription = "Manage reservation for supplier taker";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				TFlowInputData *inId = pActInfo->GetInputPort(IN_ENTITYID);
				TFlowInputData *inBuilding = pActInfo->GetInputPort(IN_BUILDINGID);
				if (inId && inBuilding)
				{
					if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(*inBuilding->GetPtr<EntityId>()))
					{
						if (SBuildingComponent *bld = pBuildingEntity->GetComponent<SBuildingComponent>())
						{
							IEntity *taker = gEnv->pEntitySystem->GetEntity(*inId->GetPtr<EntityId>());
							if (taker)
								bld->ReserveSupplierTaker(taker);
							else
								bld->ResetSupplierTaker();
						}
					}
				}
				ActivateOutput(pActInfo, OUT_TRIGGER, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_SupplierTakerReservation(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_IsSupplierReserved : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_BUILDINGID
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_IsSupplierReserved(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get supplier taker")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Entity id of the building to set")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("True", _HELP("Has reservation")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Does not have reservation")),
			{ 0 }
		};

		config.sDescription = "Get reservation for supplier taker";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				TFlowInputData *inBuilding = pActInfo->GetInputPort(IN_BUILDINGID);
				if (inBuilding)
				{
					if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(*inBuilding->GetPtr<EntityId>()))
					{
						if (SBuildingComponent *bld = pBuildingEntity->GetComponent<SBuildingComponent>())
						{
							if (bld->GetReservedSuppliesTaker())
							{
								ActivateOutput(pActInfo, OUT_TRUE, 0);
								return;
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_IsSupplierReserved(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_GetDistance : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_VECTOR1,
		IN_VECTOR2
	};
	enum Outputs
	{
		OUT_DISTANCE = 0,
		OUT_FAILED
	};
public:
	CAIFury_GetDistance(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get distance")),
			InputPortConfig<Vec3>("Vector1", _HELP("Vector first")),
			InputPortConfig<Vec3>("Vector2", _HELP("Vector second")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<float>("Distance", _HELP("Distance result")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("If anything fails")),
			{ 0 }
		};

		config.sDescription = "Get distance between two vectors";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				TFlowInputData *vec_first_data = pActInfo->GetInputPort(IN_VECTOR1);
				TFlowInputData *vec_second_data = pActInfo->GetInputPort(IN_VECTOR2);
				if (vec_first_data && vec_second_data)
				{
					const Vec3 v1 = *vec_first_data->GetPtr<Vec3>();
					const Vec3 v2 = *vec_second_data->GetPtr<Vec3>();
					ActivateOutput(pActInfo, OUT_DISTANCE, v1.GetDistance(v2));
					return;
				}
				ActivateOutput(pActInfo, OUT_FAILED, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_GetDistance(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
//Units
//WORKER
class CAIFury_UNIT_GetClosestUnfinishedBuilding : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetClosestUnfinishedBuilding(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Perform check")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns closest unfinished building found")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Checks if land owner has some unfinished buildings";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
					{
						float lastDist = 1000.f;
						for each(IUnit *pUnit in pOwner->GetUnits())
						{
							if (SBuildingComponent *pBuilding = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
							{
								if (!pBuilding->IsReady())
								{
									const float curDist = pOwnerEntity->GetWorldPos().GetDistance(pBuilding->GetEntity()->GetWorldPos());
									if (curDist < lastDist)
									{
										lastDist = curDist;
										buildingId = pBuilding->GetEntityId();
									}
								}
							}
						}
					}
				}
				if (buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetClosestUnfinishedBuilding(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_BuildBuilding : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_BuildBuilding(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Build", _HELP("Perform build")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Which building is to be built")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Succeeded", _HELP("Triggered when is building")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found or anything else fails")),
			{ 0 }
		};

		config.sDescription = "Builds specified building";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					TFlowInputData *inData = pActInfo->GetInputPort(IN_ENTITYID);
					if (inData)
					{
						EntityId buildingId = *inData->GetPtr<EntityId>();
						if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(buildingId))
						{
							if (SBuildingComponent *pBuilding = pBuildingEntity->GetComponent<SBuildingComponent>())
							{
								if (pCitizen->BuildBuilding(pBuilding))
								{
									ActivateOutput(pActInfo, OUT_TRUE, 0);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_BuildBuilding(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
}; 
class CAIFury_UNIT_ReserveBuildPoint : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_ReserveBuildPoint(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Build", _HELP("Perform build")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Which building is to be built")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<Vec3>("PointReserved", _HELP("Triggered when is succeeded")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found or anything else fails")),
			{ 0 }
		};

		config.sDescription = "Reserves and returns next possible build point";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					TFlowInputData *inData = pActInfo->GetInputPort(IN_ENTITYID);
					if (inData)
					{
						EntityId buildingId = *inData->GetPtr<EntityId>();
						if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(buildingId))
						{
							if (SBuildingComponent *pBuilding = pBuildingEntity->GetComponent<SBuildingComponent>())
							{
								const Vec3 destinationPoint = pBuilding->OccupyBuildPoint();
								if (destinationPoint != ZERO)
								{
									ActivateOutput(pActInfo, OUT_TRUE, destinationPoint);
									return;
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_ReserveBuildPoint(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
//HELPER
class CAIFury_UNIT_GetClosestBuildingWithProducts : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetClosestBuildingWithProducts(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform check")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns closest building with products found")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Checks if land owner has some building with products to be taken";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
					{
						float lastDist = 1000.f;
						for each(IUnit *pUnit in pOwner->GetUnits())
						{
							if(SBuildingComponent *pBuilding = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
							{ 
								if (pBuilding->IsReady())
								{
									if (pBuilding->HasProducts())
									{
										const float curDist = pOwnerEntity->GetWorldPos().GetDistance(pBuilding->GetEntity()->GetWorldPos());
										if (curDist < lastDist)
										{
											lastDist = curDist;
											buildingId = pBuilding->GetEntityId();
										}
									}
								}
							}
						}
					}
				}
				if (buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetClosestBuildingWithProducts(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetClosestBuildingRequiringSuppliesOfType : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_SUPPLYTYPE
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetClosestBuildingRequiringSuppliesOfType(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<int>("ResourceType", _HELP("Resource type enum")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns closest building that needs resupply")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Checks if land owner has some building that needs resupply";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					TFlowInputData *inData = pActInfo->GetInputPort(IN_SUPPLYTYPE);
					if(inData)
					{ 
						int supplyType = *inData->GetPtr<int>();
						if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
						{
							float lastDist = 1000.f;
							for each(IUnit *pUnit in pOwner->GetUnits())
							{
								if (SBuildingComponent *pBuilding = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
								{
									if (pBuilding->IsReady())
									{
										if (pBuilding->NeedSupplies() && pBuilding->GetSupplyType() == supplyType)
										{
											const float curDist = GetDistanceBetweenEntities(pOwnerEntity, pBuilding->GetEntity());
											if (curDist < lastDist)
											{
												lastDist = curDist;
												buildingId = pBuilding->GetEntityId();
											}
										}
									}
								}
							}
						}
					}
				}
				if (buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetClosestBuildingRequiringSuppliesOfType(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetClosestGranary : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetClosestGranary(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns closest building that needs resupply")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Checks if land owner has some building that needs resupply";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
					{
						float lastDist = 1000.f;
						for each(IUnit *pUnit in pOwner->GetUnits())
						{
							if (CStorageBuilding *pStorage = pUnit->GetEntity()->GetComponent<CStorageBuilding>())
							{
								if (pStorage->IsReady())
								{
									const float curDist = GetDistanceBetweenEntities(pOwnerEntity, pStorage->GetEntity());
									if (curDist < lastDist)
									{
										lastDist = curDist;
										buildingId = pStorage->GetEntityId();
									}
								}
							}
						}
					}
				}
				if (buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetClosestGranary(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_DepositCarried : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID, 
		IN_AMMOUNT,
		IN_TYPE
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_DepositCarried(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Bring", _HELP("Perform bring")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Which building goods should be dropped")),
			InputPortConfig<int>("Ammount", _HELP("How many products to drop")),
			InputPortConfig<int>("Type", _HELP("Check if type carried matches building supply type")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<int>("DropAmmount", _HELP("Outputs ammount of succesfuly dropped goods if dropped at all")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found or anything else fails")),
			{ 0 }
		};

		config.sDescription = "Drop stuff at the building";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				string entityname = "";
				if (pOwnerEntity)
					entityname = pOwnerEntity->GetName();
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					TFlowInputData *inData = pActInfo->GetInputPort(IN_ENTITYID);
					TFlowInputData *inAmmount = pActInfo->GetInputPort(IN_AMMOUNT);
					TFlowInputData *inType = pActInfo->GetInputPort(IN_TYPE);
					if (inData && inAmmount && inType)
					{
						EntityId buildingId = *inData->GetPtr<EntityId>();
						int ammount = *inAmmount->GetPtr<int>();
						int type = *inType->GetPtr<int>();
						if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(buildingId))
						{
							if (SBuildingComponent *pBuilding = pBuildingEntity->GetComponent<SBuildingComponent>())
							{
								int dropAmmount = pBuilding->GiveProducts(ammount, type);
								ActivateOutput(pActInfo, OUT_TRUE, dropAmmount);
								string buildingname = pBuildingEntity->GetName();
								return;
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_DepositCarried(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_TakeResources : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID
	};
	enum Outputs
	{
		OUT_AMMOUNT = 0,
		OUT_TYPE,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_TakeResources(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Take", _HELP("Perform take")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Which building is to be visited")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<int>("TakenAmmount", _HELP("Ammount of goods taken")),
			OutputPortConfig<int>("TakenType", _HELP("Type of goods taken")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found or anything else fails")),
			{ 0 }
		};

		config.sDescription = "Takes products from building";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				string entityname = "";
				if(pOwnerEntity)
					entityname = pOwnerEntity->GetName();
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					TFlowInputData *inData = pActInfo->GetInputPort(IN_ENTITYID);
					if (inData)
					{
						EntityId buildingId = *inData->GetPtr<EntityId>();
						if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(buildingId))
						{
							if (SBuildingComponent *pBuilding = pBuildingEntity->GetComponent<SBuildingComponent>())
							{
								int ammount = pBuilding->GrabProducts(pCitizen->GetMaxProductAmmount());
								ActivateOutput(pActInfo, OUT_AMMOUNT, ammount);
								ActivateOutput(pActInfo, OUT_TYPE, pBuilding->GetProductType());
								string buildingname = pBuildingEntity->GetName();
								return;
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_TakeResources(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetBuildingInfo: public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID
	};
	enum Outputs
	{
		OUT_GETPOS = 0,
		OUT_DROPPOS,
		OUT_SUPPLYAMMOUNT, 
		OUT_SUPPLYTYPE, 
		OUT_SUPPLYMAX,
		OUT_PRODUCTAMMOUNT, 
		OUT_PRODUCTTYPE,
		OUT_PRODUCTMAX,
		OUT_NAME,
		OUT_ISREADY,
		OUT_FAILED
	};
public:
	CAIFury_UNIT_GetBuildingInfo(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get building info")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Which building is to be checked")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<Vec3>("GetSupplyPosition", _HELP("Position assigned to building, where helpers can grab resources")),
			OutputPortConfig<Vec3>("DropSupplyPosition", _HELP("Position assigned to building, where helpers can drop resources")),
			OutputPortConfig<float>("SupplyAmmount", _HELP("Gets supplies ammount")),
			OutputPortConfig<int>("SupplyType", _HELP("Gets supplies type")),
			OutputPortConfig<float>("SupplyMaxStorage", _HELP("Gets supplies maximum ammount")),
			OutputPortConfig<int>("ProductAmmount", _HELP("Gets products ammount")),
			OutputPortConfig<int>("ProductType", _HELP("Gets product type")),
			OutputPortConfig<int>("ProductMaxStorage", _HELP("Gets product maximum ammount")),
			OutputPortConfig<string>("BuildingName", _HELP("Gets building name")),
			OutputPortConfig<bool>("IsReady", _HELP("Checks if building is fully built")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("If anything's gone wrong")),
			{ 0 }
		};

		config.sDescription = "Gets building info";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				TFlowInputData *inData = pActInfo->GetInputPort(IN_ENTITYID);
				if (inData)
				{
					EntityId buildingId = *inData->GetPtr<EntityId>();
					if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(buildingId))
					{
						if (SBuildingComponent *pBuilding = pBuildingEntity->GetComponent<SBuildingComponent>())
						{
							ActivateOutput(pActInfo, OUT_GETPOS, pBuilding->GetSupplyPos());
							ActivateOutput(pActInfo, OUT_DROPPOS, pBuilding->GetDepositPos());
							ActivateOutput(pActInfo, OUT_SUPPLYAMMOUNT, pBuilding->GetSupplyAmmount());
							ActivateOutput(pActInfo, OUT_SUPPLYTYPE, pBuilding->GetSupplyType());
							ActivateOutput(pActInfo, OUT_SUPPLYMAX, pBuilding->GetSupplyMaxAmmount());
							ActivateOutput(pActInfo, OUT_PRODUCTAMMOUNT, pBuilding->GetProductAmmount());
							ActivateOutput(pActInfo, OUT_PRODUCTTYPE, pBuilding->GetProductType());
							ActivateOutput(pActInfo, OUT_PRODUCTMAX, pBuilding->GetProductMaxAmmount());
							string nm = pBuilding->GetName();
							ActivateOutput(pActInfo, OUT_NAME, nm);
							ActivateOutput(pActInfo, OUT_ISREADY, pBuilding->IsReady());
							return;
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FAILED, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetBuildingInfo(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetClosestBuildingByName : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_NAME
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetClosestBuildingByName(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<string>("BuildingName", _HELP("Name of the building wanted")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns closest building by name")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Gets the closest building that matches the name";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
					{
						TFlowInputData *inName = pActInfo->GetInputPort(IN_NAME);
						if (inName)
						{
							const string buildingName = *inName->GetPtr<string>();
							float lastDist = 1000.f;
							for each(IUnit *pUnit in pOwner->GetUnits())
							{
								if (SBuildingComponent *pBuilding = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
								{
									if (pBuilding->GetName() == buildingName)
									{
										if (pBuilding->IsReady())
										{
											const float curDist = GetDistanceBetweenEntities(pOwnerEntity, pBuilding->GetEntity());
											if (curDist < lastDist)
											{
												lastDist = curDist;
												buildingId = pBuilding->GetEntityId();
											}
										}
									}
								}
							}
						}
					}
				}
				if (buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetClosestBuildingByName(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetBuildingWithMostProductsOfType : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_TYPE,
		IN_NOTRESERVEDONLY
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetBuildingWithMostProductsOfType(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<int>("ProductType", _HELP("Product type")),
			InputPortConfig<bool>("NotReservedOnly", _HELP("Only not reserved buildings")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns building by product type with the biggest ammount")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Gets the building that matches the product type with the biggest ammount of it";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
					{
						TFlowInputData *inType = pActInfo->GetInputPort(IN_TYPE);
						TFlowInputData *inRes = pActInfo->GetInputPort(IN_NOTRESERVEDONLY);
						if (inType && inRes)
						{
							const int productType = *inType->GetPtr<int>();
							int productAmmount = 0;
							const bool freeOnly = *inRes->GetPtr<bool>();
							for each(IUnit *pUnit in pOwner->GetUnits())
							{
								if (SBuildingComponent *pBuilding = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
								{
									if (pBuilding->IsReady() && pBuilding->GetProductType() == productType)
									{
										if (!freeOnly || freeOnly && !pBuilding->GetReservedSuppliesTaker())
										{
											const int curAmmount = pBuilding->GetProductAmmount();
											if (curAmmount > productAmmount)
											{
												productAmmount = curAmmount;
												buildingId = pBuilding->GetEntityId();
											}
										}
									}
								}
							}
						}
					}
				}
				if (buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetBuildingWithMostProductsOfType(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetBuildingWithMostNeed : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_NAME,
		IN_SKIP_MINES,
		IN_SKIP_UNITCREATORS,
		IN_SKIP_GRANARIES
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetBuildingWithMostNeed(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<string>("BuildingName", _HELP("Not necessary. If set, then only buildings that match the name will be considered")),
			InputPortConfig<bool>("SkipMines", _HELP("Skip special type")),
			InputPortConfig<bool>("SkipUnitCreators", _HELP("Skip special type")),
			InputPortConfig<bool>("SkipGranaries", _HELP("Skip special type")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns building if found")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Gets the building that needs the shortest in resources";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				EntityId buildingIdByName = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
					{
						TFlowInputData *inName = pActInfo->GetInputPort(IN_NAME);
						TFlowInputData *inMine = pActInfo->GetInputPort(IN_SKIP_MINES);
						TFlowInputData *inUnitCrt = pActInfo->GetInputPort(IN_SKIP_UNITCREATORS);
						TFlowInputData *inGranary = pActInfo->GetInputPort(IN_SKIP_GRANARIES);
						if (inName && inMine && inUnitCrt && inGranary)
						{
							const bool SkipMine = !(*inMine->GetPtr<bool>());
							const bool SkipUnitCreator = !(*inUnitCrt->GetPtr<bool>());
							const bool SkipGranary = !(*inGranary->GetPtr<bool>());

							const string buildingName = *inName->GetPtr<string>();
							float supplyAmmount = 10000.f;
							float supplyAmmountByName = 10000.f;
							for each(IUnit *pUnit in pOwner->GetUnits())
							{
								if (SBuildingComponent *pBuilding = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
								{
									if (pBuilding->IsReady())
									{
										bool bOK = true;
										CStorageBuilding *pGran = pUnit->GetEntity()->GetComponent<CStorageBuilding>();
										const bool bIsGranary = pGran != nullptr;

										if (pBuilding->IsMine() && !SkipMine)
											bOK = false;
										if (pBuilding->IsUnitCreator() && !SkipUnitCreator)
											bOK = false;
										if (bIsGranary && !SkipGranary)
											bOK = false;
										if (pBuilding->IsSupplyFull())
											bOK = false;

										if (bOK)
										{
											const float curAmmount = pBuilding->GetSupplyAmmount();
											if (curAmmount < supplyAmmount)
											{
												supplyAmmount = curAmmount;
												buildingId = pBuilding->GetEntityId();
											}
											if (pBuilding->GetName() == buildingName)
											{
												if (curAmmount < supplyAmmountByName)
												{
													supplyAmmountByName = curAmmount;
													buildingIdByName = pBuilding->GetEntityId();
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if (buildingIdByName > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingIdByName);
				else if(buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetBuildingWithMostNeed(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetBuildingWithMostProducts : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_NOTRESERVEDONLY
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_UNIT_GetBuildingWithMostProducts(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<bool>("NotReservedOnly", _HELP("Only not reserved buildings")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns building by product type with the biggest ammount")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found")),
			{ 0 }
		};

		config.sDescription = "Gets the building with the biggest ammount of product";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (CLandOwnerComponent *pOwner = pCitizen->GetOwner())
					{
						int productAmmount = 0;
						TFlowInputData *inRes = pActInfo->GetInputPort(IN_NOTRESERVEDONLY);
						if (inRes)
						{
							const bool freeOnly = *inRes->GetPtr<bool>();
							for each(IUnit *pUnit in pOwner->GetUnits())
							{
								if (SBuildingComponent *pBuilding = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
								{
									if (!freeOnly || freeOnly && !pBuilding->GetReservedSuppliesTaker())
									{
										const bool IsGranary = pBuilding->GetEntity()->GetComponent<CStorageBuilding>() != nullptr;
										if (pBuilding->IsReady() && !pBuilding->IsUnitCreator() && !IsGranary)
										{
											const int curAmmount = pBuilding->GetProductAmmount();
											if (curAmmount > productAmmount)
											{
												productAmmount = curAmmount;
												buildingId = pBuilding->GetEntityId();
											}
										}
									}
								}
							}
						}
					}
				}
				if (buildingId > 0)
					ActivateOutput(pActInfo, OUT_BUILDING, buildingId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetBuildingWithMostProducts(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetResourceAreaInfo : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID
	};
	enum Outputs
	{
		OUT_POS1 = 0,
		OUT_POS2,
		OUT_FREEPOS,
		OUT_FAILED
	};
public:
	CAIFury_UNIT_GetResourceAreaInfo(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get area info")),
			InputPortConfig<EntityId>("AreaId", _HELP("Which area is to be checked")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<Vec3>("MinePosition1", _HELP("Position assigned to area to build mine")),
			OutputPortConfig<Vec3>("MinePosition2", _HELP("Position assigned to area to build mine")),
			OutputPortConfig<Vec3>("GetFreePosition", _HELP("Position assigned to area to build mine")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("If anything's gone wrong")),
			{ 0 }
		};

		config.sDescription = "Gets area info";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				TFlowInputData *inData = pActInfo->GetInputPort(IN_ENTITYID);
				if (inData)
				{
					EntityId buildingId = *inData->GetPtr<EntityId>();
					if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(buildingId))
					{
						if (CResourceAreaComponent *area = pBuildingEntity->GetComponent<CResourceAreaComponent>())
						{
							ActivateOutput(pActInfo, OUT_POS1, area->GetMinePos(0));
							ActivateOutput(pActInfo, OUT_POS2, area->GetMinePos(1));
							ActivateOutput(pActInfo, OUT_FREEPOS, area->GetFreeSlotPos());
							return;
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FAILED, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetResourceAreaInfo(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_ReserveSupplierTaker : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_BUILDINGID
	};
	enum Outputs
	{
		OUT_TRIGGER = 0
	};
public:
	CAIFury_UNIT_ReserveSupplierTaker(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Set supplier taker")),
			InputPortConfig<EntityId>("BuildingId", _HELP("Entity id of the building to set")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnSet", _HELP("Output set")),
			{ 0 }
		};

		config.sDescription = "Manage reservation for supplier taker";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				SBuildingComponent *bld = nullptr;
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					TFlowInputData *inBuilding = pActInfo->GetInputPort(IN_BUILDINGID);
					if (inBuilding)
					{
						if (IEntity *pBuildingEntity = gEnv->pEntitySystem->GetEntity(*inBuilding->GetPtr<EntityId>()))
							bld = pBuildingEntity->GetComponent<SBuildingComponent>();
					}
					pCitizen->SetReservedBuilding(bld);
				}
				ActivateOutput(pActInfo, OUT_TRIGGER, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_ReserveSupplierTaker(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_GetReservedBuilding : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_BUILDING_ID = 0,
		OUT_FAIL
	};
public:
	CAIFury_UNIT_GetReservedBuilding(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get reserved building")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Output building id")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building was found")),
			{ 0 }
		};

		config.sDescription = "Get reservation for supplier taker";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (SBuildingComponent *res = pCitizen->GetReservedBuilding())
					{
						ActivateOutput(pActInfo, OUT_BUILDING_ID, res->GetEntityId());
						return;
					}
				}
				ActivateOutput(pActInfo, OUT_FAIL, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_GetReservedBuilding(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_AttackEnemy : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ENTITYID
	};
	enum Outputs
	{
		OUT_SUCCESS = 0,
		OUT_FAIL
	};
public:
	CAIFury_UNIT_AttackEnemy(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Attack", _HELP("Attack selected enemy")),
			InputPortConfig<EntityId>("EnemyId", _HELP("Enemy entity id")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Succeeded", _HELP("Triggers when everything's gone well")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no enemy was found")),
			{ 0 }
		};

		config.sDescription = "Deals damage to the choosen enemy";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					TFlowInputData *inData = pActInfo->GetInputPort(IN_ENTITYID);
					if (inData)
					{
						if (IEntity *pEnt = gEnv->pEntitySystem->GetEntity(*inData->GetPtr<EntityId>()))
						{
							if (SCitizenComponent *pCtz = pEnt->GetComponent<SCitizenComponent>())
							{
								pCitizen->Attack(pCtz);
								ActivateOutput(pActInfo, OUT_SUCCESS, 0);
								return;
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FAIL, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_AttackEnemy(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_UNIT_SetGetIsAtMission : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_SET = 0,
		IN_GET,
		IN_SET_VALUE
	};
	enum Outputs
	{
		OUT_TRUE = 0, 
		OUT_FALSE
	};
public:
	CAIFury_UNIT_SetGetIsAtMission(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Set", _HELP("Set is at mission")),
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Get is at mission")),
			InputPortConfig<bool>("SetValue", _HELP("Set value")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("True", _HELP("Triggers when is at mission")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Triggers when is not at mission or something fails")),
			{ 0 }
		};

		config.sDescription = "Sets or get is at mission";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_SET) || IsPortActive(pActInfo, IN_GET))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (SCitizenComponent *pCitizen = pOwnerEntity->GetComponent<SCitizenComponent>())
				{
					if (IsPortActive(pActInfo, IN_GET))
					{
						if (pCitizen->IsAtMission())
						{
							ActivateOutput(pActInfo, OUT_TRUE, 0);
							return;
						}
					}
					else if (IsPortActive(pActInfo, IN_SET))
					{
						TFlowInputData *inData = pActInfo->GetInputPort(IN_SET_VALUE);
						if (inData)
						{
							const bool val = *inData->GetPtr<bool>();
							pCitizen->SetIsAtMission(val);
							if (val)
							{
								ActivateOutput(pActInfo, OUT_TRUE, 0);
								return;
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_UNIT_SetGetIsAtMission(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
//~Units
//LAND OWNER
class CAIFury_LANDOWNER_BuildBuilding : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_NAME,
		IN_POSITION
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_LANDOWNER_BuildBuilding(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Build", _HELP("Perform build")),
			InputPortConfig<string>("BuildingName", _HELP("Name of the building to build")),
			InputPortConfig<Vec3>("BuildingPosition", _HELP("Where the building should be build")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns newly builded building id")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found of anything else fails")),
			{ 0 }
		};

		config.sDescription = "Attempts to build building by its name in specified position";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inName = pActInfo->GetInputPort(IN_NAME);
					TFlowInputData *inPosition = pActInfo->GetInputPort(IN_POSITION);
					if (inName && inPosition)
					{
						if (SBuildingComponent *newBuilding = pOwner->CreateAndBuildAtPoint(*inName->GetPtr<string>(), *inPosition->GetPtr<Vec3>()))
						{
							ActivateOutput(pActInfo, OUT_BUILDING, newBuilding->GetEntityId());
							return;
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_BuildBuilding(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_BuildCitizen : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_SCHOOLID, 
		IN_NAME
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CAIFury_LANDOWNER_BuildCitizen(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Build", _HELP("Perform build")),
			InputPortConfig<EntityId>("SchoolId", _HELP("Id of the school in which request will start")),
			InputPortConfig<string>("CitizenName", _HELP("Name of entity to create")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Succeeded", _HELP("If it was possible to build this entity")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("If something went wrong or no gold etc")),
			{ 0 }
		};

		config.sDescription = "Requests entity to build in specified school";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId buildingId = 0;
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inName = pActInfo->GetInputPort(IN_NAME);
					TFlowInputData *inSchool = pActInfo->GetInputPort(IN_SCHOOLID);
					if (inName && inSchool)
					{
						if (IEntity *pSchoolEntity = gEnv->pEntitySystem->GetEntity(*inSchool->GetPtr<EntityId>()))
						{
							if (SBuildingComponent *school = pSchoolEntity->GetComponent<SBuildingComponent>())
							{
								if (school->IsUnitCreator())
								{
									if (school->CreateUnit(*inName->GetPtr<string>()))
									{
										ActivateOutput(pActInfo, OUT_TRUE, 0);
										return;
									}
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_BuildCitizen(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_GetClosestResourceArea : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_TYPE, 
		IN_POSITION,
		IN_FREE
	};
	enum Outputs
	{
		OUT_AREAID = 0,
		OUT_FALSE
	};
public:
	CAIFury_LANDOWNER_GetClosestResourceArea(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<int>("ResourceType", _HELP("Type of the resource of area you search for")),
			InputPortConfig<Vec3>("ClosestToPosition", _HELP("Which position should this area be the closest to")),
			InputPortConfig<bool>("FreeOnly", _HELP("Look only among the areas that have some free slots")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("AreaId", _HELP("Returns area entity id")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if anything fails")),
			{ 0 }
		};

		config.sDescription = "Searches for the closest resource are to the point";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				EntityId areaId = 0;
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inType = pActInfo->GetInputPort(IN_TYPE);
					TFlowInputData *inPos = pActInfo->GetInputPort(IN_POSITION);
					TFlowInputData *inFree = pActInfo->GetInputPort(IN_FREE);
					if (inType && inPos && inFree)
					{
						const bool bFreeOnly = *inFree->GetPtr<bool>();
						const Vec3 pos = *inPos->GetPtr<Vec3>();
						float lastDist = 100000.f;
						for each(CResourceAreaComponent *area in CGamePlugin::GetResourceAreas())
						{
							if (area->GetResourceType() == *inType->GetPtr<int>())
							{
								if (!bFreeOnly || (bFreeOnly && area->HasSpaceForMine()))
								{
									const float curDist = pos.GetDistance(area->GetEntity()->GetWorldPos());
									if (curDist < lastDist)
									{
										lastDist = curDist;
										areaId = area->GetEntityId();
									}
								}
							}
						}
					}
				}
				if (areaId > 0)
					ActivateOutput(pActInfo, OUT_AREAID, areaId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_GetClosestResourceArea(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_GetAnyBuildingByName : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_NAME,
		IN_OWNERID
	};
	enum Outputs
	{
		OUT_BUILDING = 0,
		OUT_FALSE
	};
public:
	CAIFury_LANDOWNER_GetAnyBuildingByName(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<string>("BuildingName", _HELP("Name of the building to get")),
			InputPortConfig<EntityId>("LandOwnerId", _HELP("Id of the land owner of this building")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("BuildingId", _HELP("Returns fount building id")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no building found of anything else fails")),
			{ 0 }
		};

		config.sDescription = "Gets any building by name";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inName = pActInfo->GetInputPort(IN_NAME);
					TFlowInputData *inOwner = pActInfo->GetInputPort(IN_OWNERID);
					if (inName && inOwner)
					{
						if (IEntity *pOwnerEnt = gEnv->pEntitySystem->GetEntity(*inOwner->GetPtr<EntityId>()))
						{
							if (CLandOwnerComponent *lo = pOwnerEnt->GetComponent<CLandOwnerComponent>())
							{
								for each(IUnit *unit in lo->GetUnits())
								{
									if (SBuildingComponent *pBld = unit->GetEntity()->GetComponent<SBuildingComponent>())
									{
										if (pBld->GetName() == *inName->GetPtr<string>())
										{
											ActivateOutput(pActInfo, OUT_BUILDING, unit->GetEntityId());
											return;
										}
									}
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_GetAnyBuildingByName(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_GetLandOwnerInfo : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_TEAM
	};
	enum Outputs
	{
		OUT_OWNERID = 0,
		OUT_ENTIREGOLD,
		OUT_FALSE
	};
public:
	CAIFury_LANDOWNER_GetLandOwnerInfo(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<int>("Team", _HELP("Team index of the land owner we want to get")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("OwnerId", _HELP("Returns land owner id")),
			OutputPortConfig<int>("EntireGold", _HELP("Returns land owner entire gold")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("Output if no found")),
			{ 0 }
		};

		config.sDescription = "Gets land owner info of the specified team";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inTeam = pActInfo->GetInputPort(IN_TEAM);
					if (inTeam)
					{
						for each(CLandOwnerComponent *landOwner in CGamePlugin::GetLandOwners())
						{
							if (landOwner->GetTeam() == *inTeam->GetPtr<int>())
							{
								ActivateOutput(pActInfo, OUT_OWNERID, landOwner->GetEntityId());
								ActivateOutput(pActInfo, OUT_ENTIREGOLD, landOwner->GetGoldInAllGranaries());
								return;
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_GetLandOwnerInfo(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_GetOwnedBuildingCount : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_TEAM,
		IN_NAME
	};
	enum Outputs
	{
		OUT_COUNT = 0
	};
public:
	CAIFury_LANDOWNER_GetOwnedBuildingCount(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<int>("Team", _HELP("Team index of the land owner we want to get")),
			InputPortConfig<string>("BuildingName", _HELP("Name of building to search for")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<int>("Count", _HELP("Returns land owner buildings count of specified name")),
			{ 0 }
		};

		config.sDescription = "Get number of buildings that matches name of specified team";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				int count = 0;
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inTeam = pActInfo->GetInputPort(IN_TEAM);
					TFlowInputData *inName = pActInfo->GetInputPort(IN_NAME);
					if (inTeam && inName)
					{
						const string buildingName = *inName->GetPtr<string>();
						for each(CLandOwnerComponent *landOwner in CGamePlugin::GetLandOwners())
						{
							if (landOwner->GetTeam() == *inTeam->GetPtr<int>())
							{
								for each(IUnit *pUnit in landOwner->GetUnits())
								{
									if (SBuildingComponent *bld = pUnit->GetEntity()->GetComponent<SBuildingComponent>())
									{
										if (bld->GetName() == buildingName)
										{
											count++;
										}
									}
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_COUNT, count);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_GetOwnedBuildingCount(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_GetOwnedCitizenCount : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_TEAM,
		IN_NAME
	};
	enum Outputs
	{
		OUT_COUNT = 0
	};
public:
	CAIFury_LANDOWNER_GetOwnedCitizenCount(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform get")),
			InputPortConfig<int>("Team", _HELP("Team index of the land owner we want to get")),
			InputPortConfig<string>("BuildingName", _HELP("Name of building to search for")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<int>("Count", _HELP("Returns land owner buildings count of specified name")),
			{ 0 }
		};

		config.sDescription = "Get number of buildings that matches name of specified team";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				int count = 0;
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inTeam = pActInfo->GetInputPort(IN_TEAM);
					TFlowInputData *inName = pActInfo->GetInputPort(IN_NAME);
					if (inTeam && inName)
					{
						const string buildingName = *inName->GetPtr<string>();
						for each(CLandOwnerComponent *landOwner in CGamePlugin::GetLandOwners())
						{
							if (landOwner->GetTeam() == *inTeam->GetPtr<int>())
							{
								for each(IUnit *pUnit in landOwner->GetUnits())
								{
									if (SCitizenComponent *bld = pUnit->GetEntity()->GetComponent<SCitizenComponent>())
									{
										if (bld->GetName() == buildingName)
										{
											count++;
										}
									}
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_COUNT, count);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_GetOwnedCitizenCount(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_SendSoldiersTo : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_AMMOUNT,
		IN_POSITION,
		IN_FREE_ONLY
	};
	enum Outputs
	{
		OUT_SUCCESS = 0
	};
public:
	CAIFury_LANDOWNER_SendSoldiersTo(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Send", _HELP("Perform send")),
			InputPortConfig<int>("Ammount", _HELP("Ammount of soldiers to be sent")),
			InputPortConfig<Vec3>("Position", _HELP("Position for soldiers to be sent")),
			InputPortConfig<bool>("FreeWarriorsOnly", _HELP("Send only warriors that are not on the mission")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("OnSend", _HELP("If everything is ok")),
			{ 0 }
		};

		config.sDescription = "Sends soldiers to choosen place";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				int count = 0;
				std::vector<SCitizenComponent *> pList;
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					pOwner->ClearGroupSelection();
					TFlowInputData *inAmmount = pActInfo->GetInputPort(IN_AMMOUNT);
					TFlowInputData *inPosition = pActInfo->GetInputPort(IN_POSITION);
					TFlowInputData *inFree = pActInfo->GetInputPort(IN_FREE_ONLY);
					if (inAmmount && inPosition && inFree)
					{
						const bool bFreeOnly = *inFree->GetPtr<bool>();
						NavigationAgentTypeID aid = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
						const int ammount = *inAmmount->GetPtr<int>();
						const Vec3 pos = CorrectNavigationPt(aid, *inPosition->GetPtr<Vec3>());
						for each(IUnit *pUnit in pOwner->GetUnits())
						{
							if (SCitizenComponent *ctz = pUnit->GetEntity()->GetComponent<SCitizenComponent>())
							{
								if (ctz->IsWarrior())
								{
									if ((bFreeOnly && !ctz->IsAtMission()) || !bFreeOnly)
									{
										count++;
										pList.push_back(ctz);
										if (count == ammount)
										{
											for each(SCitizenComponent *pListCtz in pList)
											{
												pListCtz->MoveToLocation(pos);
												pListCtz->SetIsAtMission(true);
											}
											break;
										}
									}
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, OUT_SUCCESS, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_SendSoldiersTo(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_GetEnemyLandOwner : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_ENEMYID = 0, 
		OUT_FALSE
	};
public:
	CAIFury_LANDOWNER_GetEnemyLandOwner(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform Get")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("EnemyId", _HELP("If enemy found")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("If no enemy found")),
			{ 0 }
		};

		config.sDescription = "Gets enemy land owner entity id";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					if(CLandOwnerComponent *pEnemyLO = pOwner->GetEnemyLandOwner())
					{
						ActivateOutput(pActInfo, OUT_ENEMYID, pEnemyLO->GetEntityId());
						return;
					}
				}
				ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_GetEnemyLandOwner(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CAIFury_LANDOWNER_GetClosestEnemySoldier : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_TEAM
	};
	enum Outputs
	{
		OUT_ENEMYID = 0,
		OUT_FALSE
	};
public:
	CAIFury_LANDOWNER_GetClosestEnemySoldier(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Get", _HELP("Perform Get")),
			InputPortConfig<int>("Team", _HELP("Which team to look into")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("EnemyWarriorId", _HELP("If enemy found")),
			OutputPortConfig<SFlowSystemVoid>("Failed", _HELP("If no enemy found")),
			{ 0 }
		};

		config.sDescription = "Gets enemy warrior entity id";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				EntityId warriorId = 0;
				IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(pActInfo->pGraph->GetGraphEntity(0));
				if (CLandOwnerComponent *pOwner = pOwnerEntity->GetComponent<CLandOwnerComponent>())
				{
					TFlowInputData *inTeam = pActInfo->GetInputPort(IN_TEAM);
					if (inTeam)
					{
						const int team = *inTeam->GetPtr<int>();
						if (CLandOwnerComponent *pEnemyLO = CGamePlugin::GetLandOwnerByTeam(team))
						{
							float lastDist = 1000000.f;
							for each(IUnit *unt in pEnemyLO->GetUnits())
							{
								if (SCitizenComponent *pEnemyCitizen = unt->GetEntity()->GetComponent<SCitizenComponent>())
								{
									if (pEnemyCitizen->IsWarrior())
									{
										for each(IUnit *myUnt in pOwner->GetUnits())
										{
											if (SBuildingComponent *bld = myUnt->GetEntity()->GetComponent<SBuildingComponent>())
											{
												const string nm = "Barracks";
												if (bld->GetName() == nm)
												{
													const float curDist = bld->GetEntity()->GetWorldPos().GetDistance(pEnemyCitizen->GetEntity()->GetWorldPos());
													if (curDist < lastDist)
													{
														lastDist = curDist;
														warriorId = pEnemyCitizen->GetEntityId();
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if (warriorId > 0)
					ActivateOutput(pActInfo, OUT_ENEMYID, warriorId);
				else
					ActivateOutput(pActInfo, OUT_FALSE, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CAIFury_LANDOWNER_GetClosestEnemySoldier(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
//~LAND OWNER
//other nodes
class CVec3_EqualVector : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_VECTOR1,
		IN_VECTOR2,
		IN_TOLERANCE
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CVec3_EqualVector(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Check vector equality")),
			InputPortConfig<Vec3>("Vector1", _HELP("Vector to check")),
			InputPortConfig<Vec3>("Vector2", _HELP("Vector to check")),
			InputPortConfig<float>("Tolerance", _HELP("Vector to check tolerance")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("True", _HELP("Output trigger")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Output trigger")),
			{ 0 }
		};

		config.sDescription = "Randomizes vector around radius";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				TFlowInputData *inputVector1 = pActInfo->GetInputPort(IN_VECTOR1);
				Vec3 *inputVectorValue1 = inputVector1->GetPtr<Vec3>();
				TFlowInputData *inputVector2 = pActInfo->GetInputPort(IN_VECTOR2);
				Vec3 *inputVectorValue2 = inputVector2->GetPtr<Vec3>();
				TFlowInputData *inputTolerance = pActInfo->GetInputPort(IN_TOLERANCE);
				float *inputToleranceValue = inputTolerance->GetPtr<float>();
				if (inputVectorValue1 && inputVectorValue2 && inputToleranceValue)
				{
					const Vec3 vector = *inputVectorValue1;
					const Vec3 vector2 = *inputVectorValue2;
					/*const*/ float tolerance = *inputToleranceValue;

					const Vec3 diff = Vec3(vector.x - vector2.x, vector.y - vector2.y, vector.z);


					if (((vector.x >= vector2.x - tolerance) && (vector.x <= vector2.x + tolerance)) && ((vector.y >= vector2.y - tolerance) && (vector.y <= vector2.y + tolerance)))
					{
						//CryLogAlways("VECTOR EQUAL!");
						ActivateOutput(pActInfo, OUT_TRUE, 0);
					}
					else
					{
						//CryLogAlways("VECTOR NOT EQUAL!");
						ActivateOutput(pActInfo, OUT_FALSE, 0);
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CVec3_EqualVector(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CVec3_EqualVectorNormal : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_VECTOR1,
		IN_VECTOR2
	};
	enum Outputs
	{
		OUT_TRUE = 0,
		OUT_FALSE
	};
public:
	CVec3_EqualVectorNormal(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Check vector equality")),
			InputPortConfig<Vec3>("Vector1", _HELP("Vector to check")),
			InputPortConfig<Vec3>("Vector2", _HELP("Vector to check")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("True", _HELP("Output trigger")),
			OutputPortConfig<SFlowSystemVoid>("False", _HELP("Output trigger")),
			{ 0 }
		};

		config.sDescription = "Randomizes vector around radius";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				TFlowInputData *inputVector1 = pActInfo->GetInputPort(IN_VECTOR1);
				Vec3 *inputVectorValue1 = inputVector1->GetPtr<Vec3>();
				TFlowInputData *inputVector2 = pActInfo->GetInputPort(IN_VECTOR2);
				Vec3 *inputVectorValue2 = inputVector2->GetPtr<Vec3>();
				if (inputVectorValue1 && inputVectorValue2)
				{
					const Vec3 vector = *inputVectorValue1;
					const Vec3 vector2 = *inputVectorValue2;

					if (vector == vector2)
					{
						//CryLogAlways("VECTOR EQUAL!");
						ActivateOutput(pActInfo, OUT_TRUE, 0);
					}
					else
					{
						//CryLogAlways("VECTOR NOT EQUAL!");
						ActivateOutput(pActInfo, OUT_FALSE, 0);
					}
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CVec3_EqualVectorNormal(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CEntityId_CompareEntities : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_ID1,
		IN_ID2
	};
	enum Outputs
	{
		OUT_VALIDENTITY = 0,
		OUT_BOTH
	};
public:
	CEntityId_CompareEntities(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Check EntityID validity")),
			InputPortConfig<EntityId>("Entity1", _HELP("EntityId to check")),
			InputPortConfig<EntityId>("Entity2", _HELP("EntityId to check")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("ValidEntityId", _HELP("Outputs valid entityid out of two, if both valid, BOTH trigger is called or if none, 0 is out")),
			OutputPortConfig<SFlowSystemVoid>("Both", _HELP("Output trigger")),
			{ 0 }
		};

		config.sDescription = "Randomizes vector around radius";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				TFlowInputData *in1 = pActInfo->GetInputPort(IN_ID1);
				EntityId *inputId1 = in1->GetPtr<EntityId>();
				TFlowInputData *in2 = pActInfo->GetInputPort(IN_ID2);
				EntityId *inputId2 = in2->GetPtr<EntityId>();
				if (inputId1 && inputId2)
				{
					const EntityId id1 = *inputId1;
					const EntityId id2 = *inputId2;

					const IEntity *pEnt1 = gEnv->pEntitySystem->GetEntity(id1);
					const IEntity *pEnt2 = gEnv->pEntitySystem->GetEntity(id2);

					if (pEnt1 && pEnt2)
					{
						ActivateOutput(pActInfo, OUT_VALIDENTITY, id1);
						ActivateOutput(pActInfo, OUT_BOTH, 0);
					}
					else if (pEnt1)
						ActivateOutput(pActInfo, OUT_VALIDENTITY, id1);
					else if (pEnt2)
						ActivateOutput(pActInfo, OUT_VALIDENTITY, id2);
					else if (!pEnt1 && !pEnt2)
						ActivateOutput(pActInfo, OUT_VALIDENTITY, (EntityId)0);
				}
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CEntityId_CompareEntities(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};
class CLog_Screamer : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0,
		IN_STRING
	};
	enum Outputs
	{
		OUT_TRIGGER = 0
	};
public:
	CLog_Screamer(SActivationInfo *pActInfo)
	{
		//pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Scream", _HELP("LOG")),
			InputPortConfig<string>("Message", _HELP("Message")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Out", _HELP("Output trigger")),
			{ 0 }
		};

		config.sDescription = "Randomizes vector around radius";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				TFlowInputData *strData = pActInfo->GetInputPort(IN_STRING);

				if (strData)
				{
					string stringValue = *strData->GetPtr<string>();
					CryLogAlways("[SCREAMER] %s", stringValue);
				}
				ActivateOutput(pActInfo, OUT_TRIGGER, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CLog_Screamer(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};

//REGISTRATORS
//AIFury nodes
REGISTER_FLOW_NODE("AIFury:GetRandomNavigablePointInRadius", CAIFury_GetRandomNavigablePointInRadius);
REGISTER_FLOW_NODE("AIFury:WalkToLocation", CAIFury_AI_WalkTo);
REGISTER_FLOW_NODE("AIFury:IsWalking", CAIFury_IsMoving);
REGISTER_FLOW_NODE("AIFury:StopWalking", CAIFury_StopMoving);
REGISTER_FLOW_NODE("AIFury:LookDirection", CAIFury_LookDirection);
REGISTER_FLOW_NODE("AIFury:Fire", CAIFury_Fire);
REGISTER_FLOW_NODE("AIFury:StartWaiting", CAIFury_Wait);
REGISTER_FLOW_NODE("AIFury:StopWaiting", CAIFury_StopWaiting);
REGISTER_FLOW_NODE("AIFury:IsWaiting", CAIFury_IsWaiting);
REGISTER_FLOW_NODE("AIFury:IsLocationNavigable", CAIFury_CheckNavigationPoint);
REGISTER_FLOW_NODE("AIFury:CanSeeEntity", CAIFury_CanSeeEntity)
REGISTER_FLOW_NODE("AIFury:GetClosestSeenEnemy", CAIFury_GetClosestSeenEnemy)
REGISTER_FLOW_NODE("AIFury:AITimerManage", CAIFury_ManageAITimer)
REGISTER_FLOW_NODE("AIFury:IsPointNavigable", CAIFury_IsPointNavigable)
REGISTER_FLOW_NODE("AIFury:HasLocationChanged", CAIFury_HasLocationChanged)
REGISTER_FLOW_NODE("AIFury:GetClosestNavigablePoint", CAIFury_GetClosestNavigablePoint)
REGISTER_FLOW_NODE("AIFury:ReserveSuppliesTaker", CAIFury_SupplierTakerReservation)
REGISTER_FLOW_NODE("AIFury:IsSupplierTakerReserved", CAIFury_IsSupplierReserved)
//UNITS
//WORKER
REGISTER_FLOW_NODE("AIFury:Citizen:GetClosestUnfinishedBuilding", CAIFury_UNIT_GetClosestUnfinishedBuilding)
REGISTER_FLOW_NODE("AIFury:Building:ReserveBuildPoint", CAIFury_UNIT_ReserveBuildPoint)
REGISTER_FLOW_NODE("AIFury:Citizen:BuildBuilding", CAIFury_UNIT_BuildBuilding)
//HELPER
REGISTER_FLOW_NODE("AIFury:Citizen:GetClosestBuildingWithProducts", CAIFury_UNIT_GetClosestBuildingWithProducts)
REGISTER_FLOW_NODE("AIFury:Citizen:GetClosestBuildingRequiringSuppliesOfType", CAIFury_UNIT_GetClosestBuildingRequiringSuppliesOfType)
REGISTER_FLOW_NODE("AIFury:Citizen:GetClosestGranary", CAIFury_UNIT_GetClosestGranary)
REGISTER_FLOW_NODE("AIFury:Citizen:DepositCarriedGoods", CAIFury_UNIT_DepositCarried)
REGISTER_FLOW_NODE("AIFury:Citizen:TakeGoods", CAIFury_UNIT_TakeResources)
REGISTER_FLOW_NODE("AIFury:Building:GetInfo", CAIFury_UNIT_GetBuildingInfo)
REGISTER_FLOW_NODE("AIFury:Building:GetClosestByName", CAIFury_UNIT_GetClosestBuildingByName)
REGISTER_FLOW_NODE("AIFury:Building:GetWithMostProductsOfType", CAIFury_UNIT_GetBuildingWithMostProductsOfType)
REGISTER_FLOW_NODE("AIFury:Building:GetBuildingInBiggestNeed", CAIFury_UNIT_GetBuildingWithMostNeed)
REGISTER_FLOW_NODE("AIFury:Building:GetTheFullestBuilding", CAIFury_UNIT_GetBuildingWithMostProducts)
REGISTER_FLOW_NODE("AIFury:Area:GetResourceAreaInfo", CAIFury_UNIT_GetResourceAreaInfo)
REGISTER_FLOW_NODE("AIFury:Building:ReserveSupplierTakerCitizen", CAIFury_UNIT_ReserveSupplierTaker)
REGISTER_FLOW_NODE("AIFury:Citizen:GetReservedBuilding", CAIFury_UNIT_GetReservedBuilding)
REGISTER_FLOW_NODE("AIFury:Citizen:AttackEnemy", CAIFury_UNIT_AttackEnemy)
REGISTER_FLOW_NODE("AIFury:Citizen:GetSetIsAtMission", CAIFury_UNIT_SetGetIsAtMission)
//~Units
//LAND OWNER
REGISTER_FLOW_NODE("AIFury:LandOwner::Build", CAIFury_LANDOWNER_BuildBuilding)
REGISTER_FLOW_NODE("AIFury:LandOwner::BuildCitizen", CAIFury_LANDOWNER_BuildCitizen)
REGISTER_FLOW_NODE("AIFury:LandOwner::GetClosestResourceAreaToPoint", CAIFury_LANDOWNER_GetClosestResourceArea)
REGISTER_FLOW_NODE("AIFury:LandOwner:GetAnyBuildingByName", CAIFury_LANDOWNER_GetAnyBuildingByName)
REGISTER_FLOW_NODE("AIFury:LandOwner:GetLandOwnerInfo", CAIFury_LANDOWNER_GetLandOwnerInfo)
REGISTER_FLOW_NODE("AIFury:LandOwner:GetBuildingCountByName", CAIFury_LANDOWNER_GetOwnedBuildingCount)
REGISTER_FLOW_NODE("AIFury:LandOwner:GetCitizenCountByName", CAIFury_LANDOWNER_GetOwnedCitizenCount)
REGISTER_FLOW_NODE("AIFury:LandOwner:SendSoldiersToLocation", CAIFury_LANDOWNER_SendSoldiersTo)
REGISTER_FLOW_NODE("AIFury:LandOwner:GetEnemyLandOwner", CAIFury_LANDOWNER_GetEnemyLandOwner)
REGISTER_FLOW_NODE("AIFury:LandOwner:GetClosestEnemySoldier", CAIFury_LANDOWNER_GetClosestEnemySoldier)
//~LAND OWNER
//other nodes
REGISTER_FLOW_NODE("Vec3:GetDistance", CAIFury_GetDistance);
REGISTER_FLOW_NODE("Vec3:EqualVectorNormal", CVec3_EqualVectorNormal);
REGISTER_FLOW_NODE("Vec3:EqualVector", CVec3_EqualVector);
REGISTER_FLOW_NODE("Entity:CompareEntityId", CEntityId_CompareEntities);
REGISTER_FLOW_NODE("AIFury:Screamer", CLog_Screamer);
