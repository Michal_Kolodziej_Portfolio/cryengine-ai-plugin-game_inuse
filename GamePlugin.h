#pragma once

#include <CrySystem/ICryPlugin.h>
#include <CryGame/IGameFramework.h>
#include <CryEntitySystem/IEntityClass.h>
#include <CryNetwork/INetwork.h>

class CPlayerComponent;
struct SBuildingComponent;
class CResourceAreaComponent;
class CLandOwnerComponent;

// The entry-point of the application
// An instance of CGamePlugin is automatically created when the library is loaded
// We then construct the local player entity and CPlayerComponent instance when OnClientConnectionReceived is first called.
class CGamePlugin 
	: public ICryPlugin
	, public ISystemEventListener
	, public INetworkedClientListener
{
public:
	CRYINTERFACE_SIMPLE(ICryPlugin)
	CRYGENERATE_SINGLETONCLASS_GUID(CGamePlugin, "Game_Blank", "f01244b0-a4e7-4dc6-91e1-0ed18906fe7c"_cry_guid)

	struct UnitPublicProperties
	{
		UnitPublicProperties(string unitName, int cost, int time)
		{
			name = unitName;
			buildCost = cost;
			buildTime = time;
		}

		string name;
		int buildCost;
		int buildTime;
	};

	virtual ~CGamePlugin();
	CGamePlugin();
	// ICryPlugin
	virtual const char* GetName() const override { return "GamePlugin"; }
	virtual const char* GetCategory() const override { return "Game"; }
	virtual bool Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams) override;
	virtual void OnPluginUpdate(EPluginUpdateType updateType) override {}
	// ~ICryPlugin

	// ISystemEventListener
	virtual void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override;
	// ~ISystemEventListener

	// INetworkedClientListener
	// Sent to the local client on disconnect
	virtual void OnLocalClientDisconnected(EDisconnectionCause cause, const char* description) override {}

	// Sent to the server when a new client has started connecting
	// Return false to disallow the connection
	virtual bool OnClientConnectionReceived(int channelId, bool bIsReset) override;
	// Sent to the server when a new client has finished connecting and is ready for gameplay
	// Return false to disallow the connection and kick the player
	virtual bool OnClientReadyForGameplay(int channelId, bool bIsReset) override;
	// Sent to the server when a client is disconnected
	virtual void OnClientDisconnected(int channelId, EDisconnectionCause cause, const char* description, bool bKeepClient) override;
	// Sent to the server when a client is timing out (no packets for X seconds)
	// Return true to allow disconnection, otherwise false to keep client.
	virtual bool OnClientTimingOut(int channelId, EDisconnectionCause cause, const char* description) override { return true; }
	// ~INetworkedClientListener
	bool RegisterFlowNodes();
	bool UnregisterFlowNodes();
	void RegisterCVars();
	static void AddResourceArea(CResourceAreaComponent *pbld)
	{
		if (!pbld)
			return;

		resourceAreas.push_back(pbld);
	}
	static std::vector<CResourceAreaComponent*> GetResourceAreas() { return resourceAreas; }
	static void AddLandOwner(CLandOwnerComponent *owner)
	{
		if (!owner)
			return;

		landOwners.push_back(owner);
	}
	static std::vector<CLandOwnerComponent*> GetLandOwners() { return landOwners; }
	static int GetBuildCostByName(string name)
	{
		for each(UnitPublicProperties *unt in unitPublicProperties)
		{
			if (unt->name == name)
			{
				return unt->buildCost;
			}
		}
		return 0;
	}
	static int GetBuildTimeByName(string name)
	{
		for each(UnitPublicProperties *unt in unitPublicProperties)
		{
			if (unt->name == name)
			{
				return unt->buildTime;
			}
		}
		return 0;
	}
	static CLandOwnerComponent *GetLandOwnerByTeam(int tm);
protected:
	// Map containing player components, key is the channel id received in OnClientConnectionReceived
	std::unordered_map<int, EntityId> m_players;
	static std::vector<CResourceAreaComponent*> resourceAreas;
	static std::vector<CLandOwnerComponent*> landOwners;
	static std::vector<UnitPublicProperties*> unitPublicProperties;
};