#include "StdAfx.h"
#include "SwordComponent.h"
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "HealthComponent.h"

static void RegisterSwordComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CSwordComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterSwordComponent)

void CSwordComponent::Initialize()
{
}

uint64 CSwordComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_COLLISION) | BIT64(ENTITY_EVENT_TIMER);
}

void CSwordComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_COLLISION:
	{
		EventPhysCollision *physCollision = reinterpret_cast<EventPhysCollision *>(event.nParam[0]);
		if (physCollision)
		{
			//THIS ENTITY 
			IPhysicalEntity *pThisEntityPhysics = physCollision->pEntity[0];
			IEntity *pThisEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pThisEntityPhysics);
			IPhysicalEntity *pColliderPhysics = physCollision->pEntity[1];
			IEntity *pColliderEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pColliderPhysics);

			if (pColliderEntity && pColliderEntity != m_pEntity && pColliderEntity != pOwnerEntity)
			{
				OnCollision(pColliderEntity);
			}
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		Update(pCtx->fFrameTime);
	}
	break;
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == Timer_Attack)
		{
			bIsAttacking = false;
		}
	}
	break;
	}
}

void CSwordComponent::ReflectType(Schematyc::CTypeDesc<CSwordComponent>& desc)
{
	desc.SetGUID("{DA732A9C-8CF6-44CC-BFC7-FFFD99B0EC25}"_cry_guid);
	desc.SetEditorCategory("Weapons");
	desc.SetLabel("Sword Component");
	desc.SetDescription("Sword weapon component");
}

void CSwordComponent::Update(float frameTime)
{
	if (IPhysicalEntity *ent =  m_pEntity->GetPhysicalEntity())
	{
		int i = 0;
	}
}

void CSwordComponent::OnCollision(IEntity * pVictimEntity)
{
	if (!pVictimEntity)
		return;

	if (CHealthComponent *pVictimHealth = pVictimEntity->GetComponent<CHealthComponent>())
	{
		if (pVictimHealth->IsAlive() && bIsAttacking)
		{
			bIsAttacking = false;
			pVictimHealth->AddHealth(-20.f);

			if (pVictimHealth->GetHealth() <= 0.f)
			{
				gEnv->pEntitySystem->RemoveEntity(pVictimEntity->GetId());
			}
		}
	}
}

void CSwordComponent::StartAttack()
{
	bIsAttacking = true;
	m_pEntity->SetTimer(Timer_Attack, 1000);
}

void CSwordComponent::StopAttack()
{
	bIsAttacking = false;
	m_pEntity->KillTimer(Timer_Attack);
}
