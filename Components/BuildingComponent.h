/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AI Fury Strategy Sample
Purpose : Handles all buildings, most likely base class

--------------------------------------------------------------------------------- */

#pragma once

#include "Unit.h"
#include <CryAISystem/NavigationSystem/NavigationIdTypes.h>
#include "Resource.h"
#include "FuryResources.h"

class CHealthComponent;
class CLandOwnerComponent;
class CResourceAreaComponent;
enum class EResourceTypes;
struct SCitizenComponent;

struct SBuildingComponent : public IUnit
{
	enum TIMERS
	{
		Timer_Production = 0,
		Timer_Mine,
		Timer_Citizen_Production_Tick
	};
public:
	SBuildingComponent() = default;
	SBuildingComponent::~SBuildingComponent() {}
	// IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask_Unit() const { return BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED); }
	virtual void Initialize_Building() {}
	static void ReflectType(Schematyc::CTypeDesc<SBuildingComponent>& desc);
	// ~IEntityComponent
	void Update_Unit(float frameTime);
	void Timer_Unit(int timerId) override;
	virtual void ProcessEvent_Unit(SEntityEvent& event);
	//Building
	void Produce();
	void ProduceCitizen();
	bool CanBuildHere();
	bool IsReady() { return bIsReady; }
	void ProgressWork();
	bool Build();
	void LoadGeometry();
	void Ghost(bool valid);
	void Materialize();
	void SetupProxy();
	void Physicalize();
	void ProduceResource();
	void SupplyResource();
	bool NeedSupplies() { return currentTakeResources < maxTakeStored && !bIsMine; }
	bool HasProducts() { return currentProducts > 0; }
	virtual bool IsSupplyFull() { return currentTakeResources >= maxTakeStored; }
	virtual int GetProductAmmount() { return currentProducts; }
	virtual float GetSupplyAmmount() { return currentTakeResources; }
	virtual int GetProductType() { return (int)productType; }
	virtual int GetSupplyType() { return (int)supplyType; }
	virtual float GetSupplyMaxAmmount() { return maxTakeStored; }
	virtual int GetProductMaxAmmount() { return maxProductStored; }
	bool IsMine() { return bIsMine; }
	bool IsUnitCreator() { return bIsUnitCreator; }
	virtual int GrabProducts(int grabAmmount, int type = -1);
	virtual int GiveProducts(int giveAmmount, int type = -1);
	IUnit *CreateUnit(string unitName);
	SCitizenComponent *GetProducedCitizen() { return pProducedCitizen; }
	int GetProducedCitizenTimeLeft() { return citizenProductionTimeLeft; }

	Vec3 GetSupplyPos() { return supplyPosition; }
	Vec3 GetDepositPos(){ return depositPosition; }
	Vec3 OccupyBuildPoint();

	virtual Vec3 GetValidPositionInRadiusForBuilding(const Vec3 origin, float radius);

	virtual void ReserveSupplierTaker(IEntity *newTaker) { if (!reservedSuppliesTaker) reservedSuppliesTaker = newTaker; }
	virtual void ResetSupplierTaker() { reservedSuppliesTaker = nullptr; }
	virtual IEntity *GetReservedSuppliesTaker() { return reservedSuppliesTaker; }
	//~Building
protected:
	void BuildFully();
	void ExcludeFromNavigation();

	int buildProgress = 0;
	bool bIsReady = false;
	bool bReadyByDefault = false;
	bool bReadyByDefault_Last = false;
	//editor adjustable
	Schematyc::GeomFileName geometryPath;
	Schematyc::MaterialFileName materialPath;
	Vec3 buildPos = ZERO;
	NavigationMeshID navMeshID;
	bool bIsProducing = false;
	bool bIsInvalidPlacement = false;
	bool bLastPlacementValidity = false;
	CResourceAreaComponent *pResourceArea = nullptr;
	SCitizenComponent *pProducedCitizen = nullptr;
	int citizenProductionTimeLeft = 0;
	bool bAutoProduction = false;
	//resource management

	EResourceTypes productType;
	EResourceTypes supplyType;
	int maxProductStored = 0;
	int currentProducts = 0;
	float maxTakeStored = 0.f;
	float currentTakeResources = 0.f;
	int productionTick = 0;
	float supplyPerProduct = 0.f;
	float resourceMinedAtOnce = 0.f;
	bool bIsMine = false;
	bool bIsUnitCreator = false;
	bool bIsSettled = false;
	IEntity *reservedSuppliesTaker = nullptr;

	//positioning of units
	Vec3 supplyPosition = ZERO;
	Vec3 depositPosition = ZERO;
	Vec3 unitSpawnPosition = ZERO;

	bool buildOccupancy[24] = { false };
};
