#include "StdAfx.h"
#include "CitizenComponent.h"
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "CryRenderer/IRenderer.h"
#include "GlobalResources.h"
#include "HealthComponent.h"
#include "AICharacterComponent.h"
#include "CryMath/Random.h"
#include "SwordComponent.h"

static void RegisterCitizenComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(SCitizenComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterCitizenComponent)

SCitizenComponent::~SCitizenComponent()
{
}

void SCitizenComponent::Initialize()
{
	highlightRadius = 3.f;
	highlightCircleColor = Col_Yellow;
}

uint64 SCitizenComponent::GetEventMask_Unit() const
{
	return BIT64(ENTITY_EVENT_TIMER);
}

void SCitizenComponent::ProcessEvent_Unit(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == Timer_Build_Tick)
		{
			if (bIsWorking)
			{
				bIsWorking = false;
			}
		}
	}
	break;
	}
}

void SCitizenComponent::ReflectType(Schematyc::CTypeDesc<SCitizenComponent>& desc)
{
	desc.SetGUID("{E5B1F7EC-458B-4753-B7C6-73ECB4011042}"_cry_guid);
	desc.AddBase<IUnit>();
	desc.SetEditorCategory("Game");
	desc.SetLabel("Citizen Component");
	desc.SetDescription("Handles citizen logic");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//editor properties
	desc.AddMember(&SCitizenComponent::unitProperties, 'unit', "UnitProperties", "Unit settings", "Settings of this unit", SUnitProperties());
	desc.AddMember(&SCitizenComponent::bIsWarrior, 'iswa', "IsWarrior", "Is unit warrior", "Is unit warrior type", false);
}

void SCitizenComponent::Update_Unit(float frameTime)
{
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(m_pEntity->GetWorldPos(), 1.f, Col_Azure);

	if (!pAIComponent)
	{
		pAIComponent = m_pEntity->GetComponent<CAICharacterComponent>();
	}
	else if(SPathfindingComponent *pth = pAIComponent->GetPathfinder())
	{
		if (!pth->IsMoving() && !bIsStucked)
		{
			bIsStucked = true;
			m_pEntity->SetTimer(Timer_Stucked, 5000);
		}
		else if (pth->IsMoving() && bIsStucked)
		{
			bIsStucked = false;
			m_pEntity->KillTimer(Timer_Stucked);
		}
	}
	if (pAIComponent)
	{
		if (bIsWarrior && !bWarriorInitialized && m_pEntity->GetPhysicalEntity())
		{
			bWarriorInitialized = true;
			InitializeWarrior();
		}
	}
}

void SCitizenComponent::Timer_Unit(int timerId)
{
	if (timerId == Timer_Stucked)
	{
		SetIsAtMission(false);
		SetReservedBuilding(nullptr);
		bIsStucked = false;
		if (pAIComponent)
		{
			if (pAIComponent->GetPathfinder())
			{
				pAIComponent->GetPathfinder()->Restart();
			}
		}
	}
	if (timerId == Timer_Attack)
	{
		bCanAttack = true;
	}
}

bool SCitizenComponent::BuildBuilding(SBuildingComponent * pBld)
{
	if (!pBld || !pAIComponent)
		return false;
	if (bIsWorking)
		return true;

	bIsWorking = true;

	if (!pBld->IsReady())
	{
		m_pEntity->SetTimer(Timer_Build_Tick, 1000);
		pBld->ProgressWork();
		return true;
	}
	else
	{
		m_pEntity->KillTimer(Timer_Build_Tick);
		bIsWorking = false;
		return false;
	}
	
	return false;
}

void SCitizenComponent::SetReservedBuilding(SBuildingComponent * pBld)
{
	if (pReservedBuildingToGrabFrom)
		pReservedBuildingToGrabFrom->ResetSupplierTaker();

	pReservedBuildingToGrabFrom = pBld;

	if (pReservedBuildingToGrabFrom)
		pReservedBuildingToGrabFrom->ReserveSupplierTaker(m_pEntity);
	
}

void SCitizenComponent::MoveToLocation(const Vec3 pos)
{
	if (pAIComponent)
	{
		pAIComponent->GoTo(pos);
	}
}

void SCitizenComponent::InitializeWarrior()
{
	bCanAttack = true;
	SEntitySpawnParams spawnSword;
	if (IEntityClass *pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("schematyc::schematycentities::sword"))
	{
		spawnSword.pClass = pClass;
		if (IEntity *pSwordEntity = gEnv->pEntitySystem->SpawnEntity(spawnSword))
		{
			CEntityAttachment *pEntityAttachment = new CEntityAttachment();
			pEntityAttachment->SetEntityId(pSwordEntity->GetId());

			if (ICharacterInstance *pCharacter = pAIComponent->GetAnimations()->GetCharacter())
			{
				if (IAttachmentManager *pAttachmentManager = pCharacter->GetIAttachmentManager())
				{
					if (IAttachment *pAttachment = pAttachmentManager->GetInterfaceByName("sword"))
					{
						pAttachment->AddBinding(pEntityAttachment);

						pAIComponent->SetHasWeapon(true);

						if (CSwordComponent *sword = pSwordEntity->GetComponent<CSwordComponent>())
						{
							sword->SetOwner(m_pEntity);
						}
					}
				}
			}
		}
	}
}

void SCitizenComponent::Attack(SCitizenComponent * pEnemy)
{
	if (!pEnemy || !bCanAttack)
		return;

	if (CHealthComponent *pEnemyHealth = pEnemy->GetEntity()->GetComponent<CHealthComponent>())
	{
		pEnemyHealth->AddHealth(-20.f);
		m_pEntity->SetTimer(Timer_Attack, 1000);
		bCanAttack = false;

		if (!pEnemy->GetOwner())
		{
			CLandOwnerComponent *pMissingOwner = CGamePlugin::GetLandOwnerByTeam(pEnemy->GetTeam());
			pEnemy->SetOwner(pMissingOwner);
		}
		if (pEnemy->GetOwner())
			pEnemy->GetOwner()->SetEnemyLandOwner(pLandOwner);

		if (pEnemyHealth->GetHealth() <= 0.f)
		{
			bCanAttack = true;
			m_pEntity->KillTimer(Timer_Attack);

			if (pEnemy->GetOwner())
			{
				pEnemy->GetOwner()->RemoveUnit(pEnemy);
				pEnemy->GetOwner()->GroupDeselectUnit(pEnemy);
			}
			pEnemy->GetEntity()->RemoveAllComponents();
			gEnv->pEntitySystem->RemoveEntity(pEnemy->GetEntityId(), true);
		}
	}
}
