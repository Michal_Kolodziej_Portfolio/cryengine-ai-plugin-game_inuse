/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AI Fury Strategy Game Sample
Purpose : Handles entire player/ai player logic

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

struct SBuildingComponent;
struct SCitizenComponent;
struct IUnit;

class CLandOwnerComponent : public IEntityComponent
{
public:
	CLandOwnerComponent() = default;
	CLandOwnerComponent::~CLandOwnerComponent() {}
	// IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CLandOwnerComponent>& desc);
	// ~IEntityComponent
	void Update(float frameTime);
	//LandOwner
	//Buildings management
	void BearTheCosts(int cost);
	int GetGoldInAllGranaries();
	IUnit *CreateUnit(string unitName);
	SBuildingComponent *CreateBuilding(string buildingName);
	SBuildingComponent *Build();
	SBuildingComponent *CreateAndBuildAtPoint(string buildingName, const Vec3 pos);
	void SelectUnit(IUnit *pUnit);
	void GroupSelectUnit(IUnit *pUnit);
	void GroupDeselectUnit(IUnit *pUnit);
	void ClearGroupSelection();
	void AddUnit(IUnit *pUnit);
	void RemoveUnit(IUnit *pUnit);
	SBuildingComponent *GetCreatingBuilding() { return pCreatingBuilding; }
	IUnit *GetSelectedUnit() { return pSelectedUnit; }
	void SetGroupMid(IUnit *pUnit) { pGroupMid = pUnit; }
	IUnit *GetGroupMid() { return pGroupMid; }
	std::vector<IUnit*> GetGroupSelected() { return pUnitsSelection; }
	std::vector<IUnit*> GetUnits() { return pUnits; }
	std::vector<SBuildingComponent*> GetBuildings();
	int GetTeam() { return team; }
	void SetEnemyLandOwner(CLandOwnerComponent *enm) { pEnemyLandOwner = enm; }
	CLandOwnerComponent *GetEnemyLandOwner() { return pEnemyLandOwner; }
	//~LandOwner
protected:
	SBuildingComponent *pCreatingBuilding = nullptr;
	IUnit *pSelectedUnit = nullptr;
	std::vector<IUnit*> pUnits;
	std::vector<IUnit*> pUnitsSelection;
	IUnit *pGroupMid = nullptr;
	int team;
	CLandOwnerComponent *pEnemyLandOwner = nullptr;
};