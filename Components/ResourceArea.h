/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AI Fury Strategy Game Sample
Purpose : Handles resources on the map

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <DefaultComponents/Geometry/StaticMeshComponent.h>

struct SBuildingComponent;

class CResourceAreaComponent : public IEntityComponent
{
public:
	CResourceAreaComponent() = default;
	CResourceAreaComponent::~CResourceAreaComponent() {}
	// IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CResourceAreaComponent>& desc);
	// ~IEntityComponent
	void Update(float frameTime);
	//Resource
	void Add(float add) { currentUnits += add; }
	float Get() { return currentUnits; }
	void Set(float set) { currentUnits = set; }
	float GetMax() { return maxUnits; }
	void SetMax(float set) { maxUnits = set; }
	bool Utilize(float ammount);
	int GetResourceType() { return (int)resourceType; }
	float GetAreaRadius() { return areaRadius; }
	void AddMine(SBuildingComponent *pMine);
	bool HasSpaceForMine() { return (!pMines[0] || !pMines[1]); }
	Vec3 GetFreeSlotPos() { if (!pMines[0]) return mineSlotPos[0]; if (!pMines[1]) return mineSlotPos[1]; return ZERO; }
	Vec3 GetMinePos(int id) { return mineSlotPos[id]; }
	//~Resource
protected:
	float currentUnits = 0.f;
	float maxUnits = 0.f;
	float areaRadius = 0.f;
	ColorF areaColor = ColorF(0.f, 0.f, 0.f);
	EResourceTypes resourceType;
	SBuildingComponent *pMines[2] = { nullptr };
	Vec3 mineSlotPos[2] = { ZERO };
};
