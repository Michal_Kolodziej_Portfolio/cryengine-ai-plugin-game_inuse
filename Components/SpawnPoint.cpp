#include "StdAfx.h"
#include "SpawnPoint.h"

#include <CrySchematyc/Reflection/TypeDesc.h>
#include <CrySchematyc/Utils/EnumFlags.h>
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include <CrySchematyc/Env/Elements/EnvFunction.h>
#include <CrySchematyc/Env/Elements/EnvSignal.h>
#include <CrySchematyc/ResourceTypes.h>
#include <CrySchematyc/MathTypes.h>
#include <CrySchematyc/Utils/SharedString.h>
#include "GamePlugin.h"
#include "LandOwner.h"
#include "CitizenComponent.h"

static void RegisterSpawnPointComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CSpawnPointComponent));
		// Functions
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterSpawnPointComponent)

void CSpawnPointComponent::Initialize()
{
	if (!gEnv->IsEditor())
	{
		string className = "";
		if (unitType == UnitType::Worker)
			className = "schematyc::schematycentities::worker";
		else if (unitType == UnitType::Helper)
			className = "schematyc::schematycentities::helper";
		else if (unitType == UnitType::Warrior)
			className = "schematyc::schematycentities::warrior";

		IEntityClass *pEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(className);
		if (pEntityClass)
		{
			SEntitySpawnParams spawn;
			spawn.pClass = pEntityClass;
			spawn.vPosition = m_pEntity->GetWorldPos();
			spawn.qRotation = m_pEntity->GetWorldRotation();
			if (IEntity *pUnitEntity = gEnv->pEntitySystem->SpawnEntity(spawn))
			{
				if (SCitizenComponent *ctz = pUnitEntity->GetComponent<SCitizenComponent>())
				{
					ctz->SetTeam(team);
					if (CLandOwnerComponent *lo = CGamePlugin::GetLandOwnerByTeam(team))
					{
						ctz->SetOwner(lo);
						lo->AddUnit(ctz);
					}
				}
			}
		}
	}
}

void CSpawnPointComponent::ReflectType(Schematyc::CTypeDesc<CSpawnPointComponent>& desc)
{
	desc.SetGUID("{41316132-8A1E-4073-B0CD-A242FD3D2E90}"_cry_guid);
	desc.SetEditorCategory("Game");
	desc.SetLabel("SpawnPoint");
	desc.SetDescription("This spawn point can be used to spawn entities");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });

	desc.AddMember(&CSpawnPointComponent::team, 'team', "Team", "Team", "Team", 0);
	desc.AddMember(&CSpawnPointComponent::unitType, 'utyp', "UnitType", "Unit type", "Type of the unit", UnitType());
}

void CSpawnPointComponent::SpawnEntity(IEntity* otherEntity)
{
	otherEntity->SetWorldTM(m_pEntity->GetWorldTM());
}
