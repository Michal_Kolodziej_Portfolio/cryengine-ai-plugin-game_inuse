#include "StdAfx.h"
#include "Player.h"

#include <CryRenderer/IRenderAuxGeom.h>
#include "SpawnPoint.h"
#include "CryInput/IHardwareMouse.h"
#include "BuildingComponent.h"
#include "LandOwner.h"
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "AICharacterComponent.h"
#include "CryAISystem/INavigationSystem.h"
#include "CryAISystem/NavigationSystem/MNMNavMesh.h"
#include "GlobalResources.h"
#include "HudComponent.h"
#include "HealthComponent.h"
#include "CitizenComponent.h"
#include "GamePlugin.h"

static void RegisterPlayerComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CPlayerComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterPlayerComponent)

string CPlayerComponent::UNIT_CREATE = "NOUNIT";

void CPlayerComponent::Initialize()
{
	m_pCameraComponent = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CCameraComponent>();
	CreateHud();
	m_pInputComponent = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CInputComponent>();

	m_pInputComponent->RegisterAction("player", "moveleft", [this](int activationMode, float value) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveLeft, activationMode);  });
	m_pInputComponent->BindAction("player", "moveleft", eAID_KeyboardMouse, EKeyId::eKI_A);

	m_pInputComponent->RegisterAction("player", "moveright", [this](int activationMode, float value) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveRight, activationMode);  });
	m_pInputComponent->BindAction("player", "moveright", eAID_KeyboardMouse, EKeyId::eKI_D);

	m_pInputComponent->RegisterAction("player", "moveforward", [this](int activationMode, float value) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveForward, activationMode);  });
	m_pInputComponent->BindAction("player", "moveforward", eAID_KeyboardMouse, EKeyId::eKI_W);

	m_pInputComponent->RegisterAction("player", "moveback", [this](int activationMode, float value) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveBack, activationMode);  });
	m_pInputComponent->BindAction("player", "moveback", eAID_KeyboardMouse, EKeyId::eKI_S);

	m_pInputComponent->RegisterAction("player", "mouse_rotateyaw", [this](int activationMode, float value) { m_mouseDeltaRotation.x -= value; });
	m_pInputComponent->BindAction("player", "mouse_rotateyaw", eAID_KeyboardMouse, EKeyId::eKI_MouseX);

	m_pInputComponent->RegisterAction("player", "mouse_rotatepitch", [this](int activationMode, float value) { m_mouseDeltaRotation.y -= value; });
	m_pInputComponent->BindAction("player", "mouse_rotatepitch", eAID_KeyboardMouse, EKeyId::eKI_MouseY);

	m_pInputComponent->RegisterAction("player", "cam_rotate", [this](int activationMode, float value) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveRotate, activationMode);  });
	m_pInputComponent->BindAction("player", "cam_rotate", eAID_KeyboardMouse, EKeyId::eKI_Mouse3);

	m_pInputComponent->RegisterAction("player", "cam_zoom_in", [this](int activationMode, float value) { if(activationMode == eIS_Pressed) Action_CameraZoomIn(); });
	m_pInputComponent->BindAction("player", "cam_zoom_in", eAID_KeyboardMouse, EKeyId::eKI_MouseWheelUp);

	m_pInputComponent->RegisterAction("player", "cam_zoom_out", [this](int activationMode, float value) { if (activationMode == eIS_Pressed) Action_CameraZoomOut(); });
	m_pInputComponent->BindAction("player", "cam_zoom_out", eAID_KeyboardMouse, EKeyId::eKI_MouseWheelDown);

	m_pInputComponent->RegisterAction("player", "create_building", [this](int activationMode, float value) { if (activationMode == eIS_Pressed) CreateUnit("Gold_Mine"); });
	m_pInputComponent->BindAction("player", "create_building", eAID_KeyboardMouse, EKeyId::eKI_1);

	m_pInputComponent->RegisterAction("player", "create_worker", [this](int activationMode, float value) { if (activationMode == eIS_Pressed) CreateUnit("Worker"); });
	m_pInputComponent->BindAction("player", "create_worker", eAID_KeyboardMouse, EKeyId::eKI_2);

	m_pInputComponent->RegisterAction("player", "action_key", [this](int activationMode, float value) { Action(activationMode); });
	m_pInputComponent->BindAction("player", "action_key", eAID_KeyboardMouse, EKeyId::eKI_Mouse1);

	m_pInputComponent->RegisterAction("player", "warrior_action", [this](int activationMode, float value) { WarriorAction(activationMode); });
	m_pInputComponent->BindAction("player", "warrior_action", eAID_KeyboardMouse, EKeyId::eKI_Mouse2);

	m_pInputComponent->RegisterAction("player", "debug", [this](int activationMode, float value) { if (activationMode == eIS_Pressed) DebugDraw(); });
	m_pInputComponent->BindAction("player", "debug", eAID_KeyboardMouse, EKeyId::eKI_L);

	m_pInputComponent->RegisterAction("player", "hud_toggle", [this](int activationMode, float value) { if (activationMode == eIS_Pressed) pHud->Toggle(); });
	m_pInputComponent->BindAction("player", "hud_toggle", eAID_KeyboardMouse, EKeyId::eKI_H);

	m_pInputComponent->RegisterAction("player", "rotate_building_right", [this](int activationMode, float value) { if (activationMode == eIS_Pressed || activationMode == eIS_Down) RotateBuilding(0); });
	m_pInputComponent->BindAction("player", "rotate_building_right", eAID_KeyboardMouse, EKeyId::eKI_Right);

	m_pInputComponent->RegisterAction("player", "rotate_building_left", [this](int activationMode, float value) { if (activationMode == eIS_Pressed || activationMode == eIS_Down) RotateBuilding(1); });
	m_pInputComponent->BindAction("player", "rotate_building_left", eAID_KeyboardMouse, EKeyId::eKI_Left);
}

uint64 CPlayerComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE);
}

void CPlayerComponent::ProcessEvent(SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		if (UNIT_CREATE != "NOUNIT")
		{
			CreateUnit(UNIT_CREATE);
			UNIT_CREATE = "NOUNIT";
		}
		if (!pLandOwner)
		{
			pLandOwner = m_pEntity->GetComponent<CLandOwnerComponent>();
		}
		else
		{
			if (IUnit *pSelectedUnit = pLandOwner->GetSelectedUnit())
			{
				if (SBuildingComponent *pBuilding = pSelectedUnit->GetEntity()->GetComponent<SBuildingComponent>())
				{
					if (pBuilding->IsReady())
					{
						if (CHealthComponent *pBuildingHealth = pBuilding->GetEntity()->GetComponent<CHealthComponent>())
						{
							string prod = RESOURCE_NAME[pBuilding->GetProductType()] + ": " + ToString(pBuilding->GetProductAmmount());
							string sup = RESOURCE_NAME[pBuilding->GetSupplyType()] + ": " + ToString(pBuilding->GetSupplyAmmount());
							string hlth = ToString(pBuildingHealth->GetHealth()) + " : " + ToString(pBuildingHealth->GetMaxHealth());
							string mak = "";
							if (SCitizenComponent *ctz = pBuilding->GetProducedCitizen())
							{
								string nm = ctz->GetName();
								mak = "Creating: " + nm + " Time left: " + ToString(pBuilding->GetProducedCitizenTimeLeft());
							}
							pHud->UpdateUnitInfo(prod, sup, hlth, mak);
						}
					}
				}
			}
			char bf[32];
			string gld_str = itoa(pLandOwner->GetGoldInAllGranaries(), bf, 10);
			string gld = "Gold in all granaries: " + gld_str;
			pHud->UpdateTextField("Gold", gld, 1500, 930, 30, true);
		}
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		//calculate mouse position in the game world
		float mouseX, mouseY;
		gEnv->pHardwareMouse->GetHardwareMouseClientPosition(&mouseX, &mouseY);

		// Invert mouse Y
		mouseY = gEnv->pRenderer->GetHeight() - mouseY;

		Vec3 vPos0(0, 0, 0);
		gEnv->pRenderer->UnProjectFromScreen(mouseX, mouseY, 0, &vPos0.x, &vPos0.y, &vPos0.z);

		Vec3 vPos1(0, 0, 0);
		gEnv->pRenderer->UnProjectFromScreen(mouseX, mouseY, 1, &vPos1.x, &vPos1.y, &vPos1.z);

		Vec3 vDir = vPos1 - vPos0;
		vDir.Normalize();

		const auto rayFlags = rwi_stop_at_pierceable | rwi_colltype_any;
		ray_hit hit;
		bool bZeroBuilding = true;
		IPhysicalEntity *pCreatingBuiltingPhys = nullptr;
		if (pLandOwner && pLandOwner->GetCreatingBuilding())
		{
			pCreatingBuiltingPhys = pLandOwner->GetCreatingBuilding()->GetEntity()->GetPhysicalEntity();
		}
		if (gEnv->pPhysicalWorld->RayWorldIntersection(vPos0, vDir * gEnv->p3DEngine->GetMaxViewDistance(), ent_all, rayFlags, &hit, 1, pCreatingBuiltingPhys))
		{
			mouseTargetPos = hit.pt;

			if (pLandOwner && pLandOwner->GetCreatingBuilding())
				pLandOwner->GetCreatingBuilding()->GetEntity()->SetPos(hit.pt);

			//gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(hit.pt, 1.f, Col_Azure);

			if (hit.pCollider)
			{
				if (IEntity *pCollider = gEnv->pEntitySystem->GetEntityFromPhysics(hit.pCollider))
				{
					if (!pLandOwner || !pLandOwner->GetCreatingBuilding())
					{
						if (pTargetUnit = pCollider->GetComponent<IUnit>())
						{
							bZeroBuilding = false;
						}
					}
				}
			}
		}
		if (bZeroBuilding)
		{
			IPhysicalEntity **pList = NULL;
			int num = gEnv->pEntitySystem->GetPhysicalEntitiesInBox(mouseTargetPos, 2.f, pList);
			for (int i = 0; i < num; i++)
			{
				if (IEntity *pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pList[i]))
				{
					if (!pLandOwner || !pLandOwner->GetCreatingBuilding())
					{
						if (pTargetUnit = pEnt->GetComponent<IUnit>())
						{
							bZeroBuilding = false;
							break;
						}
					}
				}
			}
			if (bZeroBuilding)
				pTargetUnit = nullptr;
		}

		const float moveSpeed = 100.5f;
		Vec3 velocity = ZERO;

		if (m_inputFlags & (TInputFlags)EInputFlag::MoveLeft)
		{
			velocity.x -= moveSpeed * pCtx->fFrameTime;
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveRight)
		{
			velocity.x += moveSpeed * pCtx->fFrameTime;
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveForward)
		{
			velocity.y += moveSpeed * pCtx->fFrameTime;
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveBack)
		{
			velocity.y -= moveSpeed * pCtx->fFrameTime;
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveRotate)
		{
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(lookOrientation));
			Matrix34 cameraTransform = IDENTITY;
			cameraTransform.SetRotation33(CCamera::CreateOrientationYPR(ypr));
			cameraTransform.SetTranslation(Vec3(0.f, camYOffset, camZOffset));
			m_pCameraComponent->SetTransformMatrix(cameraTransform);

			Ang3 yprCam = CCamera::CreateAnglesYPR(Matrix33(m_pEntity->GetWorldRotation()));
			yprCam.x += m_mouseDeltaRotation.x * rotationSpeed;
			Quat entityRotation = Quat(CCamera::CreateOrientationYPR(yprCam));
			m_pEntity->SetRotation(entityRotation);
			m_mouseDeltaRotation = ZERO;
		}
		else
		{
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(lookOrientation));
			Matrix34 cameraTransform = IDENTITY;
			cameraTransform.SetRotation33(CCamera::CreateOrientationYPR(ypr));
			cameraTransform.SetTranslation(Vec3(0.f, camYOffset, camZOffset));
			m_pCameraComponent->SetTransformMatrix(cameraTransform);

			Vec3 lookDir = Vec3(0.f, 0.016528f, -1.f);
			lookOrientation = Quat::CreateRotationVDir(lookDir);
		}
		Vec3 pos = m_pEntity->GetWorldPos();
		pos += m_pEntity->GetWorldRotation() * velocity;
		pos.z = gEnv->p3DEngine->GetTerrainElevation(pos.x, pos.y);
		m_pEntity->SetPos(pos);
	}
	break;
	}
}

void CPlayerComponent::ReflectType(Schematyc::CTypeDesc<CPlayerComponent>& desc)
{
	desc.SetGUID("{5E47A2CC-2A4F-40BE-9DBB-1EB3DC0C6E22}"_cry_guid);
	desc.SetEditorCategory("Game");
	desc.SetLabel("Player Component");
	desc.SetDescription("Handles human player functionality");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
}

void CPlayerComponent::CVarHandler(IConsoleCmdArgs * pArgs)
{
	if (pArgs->GetArgCount() > 0)
	{
		const string unitName = pArgs->GetArg(1);
		UNIT_CREATE = unitName;
	}
}

void CPlayerComponent::Revive()
{
	if (gEnv->IsEditor())
		return;

	auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
	pEntityIterator->MoveFirst();

	while (!pEntityIterator->IsEnd())
	{
		IEntity *pEntity = pEntityIterator->Next();

		if (auto* pSpawner = pEntity->GetComponent<CSpawnPointComponent>())
		{
			pSpawner->SpawnEntity(m_pEntity);
			break;
		}
	}
	GetEntity()->Hide(false);
	m_inputFlags = 0;
	m_mouseDeltaRotation = ZERO;
}

void CPlayerComponent::Action_CameraZoomIn()
{
	if (zoomRatio > maxZoom)
	{
		zoomRatio -= 1.f;
		camYOffset -= minCamYOffset * zoomStep;
		camZOffset -= minCamZOffset * zoomStep;
	}
}

void CPlayerComponent::Action_CameraZoomOut()
{
	if (zoomRatio < minZoom)
	{
		zoomRatio += 1.f;
		camYOffset += minCamYOffset * zoomStep;
		camZOffset += minCamZOffset * zoomStep;
	}
}

void CPlayerComponent::Action(int actMode)
{
	if (actMode == eIS_Pressed)
	{
		hgRectStartPos = mouseTargetPos;
		if (pLandOwner)
		{
			float pfX, pfY;
			gEnv->pHardwareMouse->GetHardwareMousePosition(&pfX, &pfY);
			if (pfY < 850.f)
			{
				if (pTargetUnit)
					SelectUnit(pTargetUnit);

				else if (pLandOwner->GetCreatingBuilding())
					Build();

				else
				{
					SelectUnit(nullptr);
				}
			}
		}
	}
	else if (actMode == eIS_Released)
	{
		pLandOwner->ClearGroupSelection();

		Vec3 diff = hgRectStartPos - hgRectCurPos;
		Vec3 boxOrigin = Vec3(hgRectStartPos.x - (diff.x * 0.5f), hgRectStartPos.y - (diff.y * 0.5f), hgRectStartPos.z - (diff.z * 0.5f));

		const float x_rad = (diff.x < 0) ? diff.x * (-1) : diff.x;
		const float y_rad = (diff.y < 0) ? diff.y * (-1) : diff.y;
		const float fRadius = (x_rad > y_rad) ? x_rad : y_rad;
		
		const string wr = "Warrior";

		float last_dist = 1000000.f;

		IPhysicalEntity **list = NULL;

		int num = gEnv->pEntitySystem->GetPhysicalEntitiesInBox(boxOrigin, fRadius, list);

		for (int i = 0; i < num; i++)
		{
			if (list[i])
			{
				if (IEntity *pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(list[i]))
				{
					const float up_x = (hgRectStartPos.x > hgRectCurPos.x) ? hgRectStartPos.x : hgRectCurPos.x;
					const float up_y = (hgRectStartPos.y > hgRectCurPos.y) ? hgRectStartPos.y : hgRectCurPos.y;
					const float down_x = (hgRectStartPos.x < hgRectCurPos.x) ? hgRectStartPos.x : hgRectCurPos.x;
					const float down_y = (hgRectStartPos.y < hgRectCurPos.y) ? hgRectStartPos.y : hgRectCurPos.y;

					const Vec3 entPos = pEnt->GetWorldPos();

					if ((entPos.x < up_x && entPos.x > down_x) && (entPos.y < up_y && entPos.y > down_y))
					{
						if (SCitizenComponent *ctz = pEnt->GetComponent<SCitizenComponent>())
						{
							if (ctz->GetName() == wr)
							{
								pLandOwner->GroupSelectUnit(ctz);
								if (pEnt->GetWorldPos().GetDistance(boxOrigin) < last_dist)
								{
									last_dist = pEnt->GetWorldPos().GetDistance(boxOrigin);
									pLandOwner->SetGroupMid(ctz);
								}
							}
						}
					}
				}
			}
		}
	}
	else if (actMode = eIS_Down)
	{
		hgRectCurPos = mouseTargetPos;

		if (hgRectStartPos != hgRectCurPos)
		{
			Vec3 pts[4];
			pts[0] = hgRectStartPos;
			pts[1] = Vec3(hgRectStartPos.x, hgRectCurPos.y, hgRectStartPos.z);
			pts[2] = hgRectCurPos;
			pts[3] = Vec3(hgRectCurPos.x, hgRectStartPos.y, hgRectCurPos.z);

			gEnv->pRenderer->GetIRenderAuxGeom()->DrawPolyline(pts, 4, true, Col_Gold, 10.f);
		}
	}
}

void CPlayerComponent::WarriorAction(int actMode)
{
	if (pLandOwner)
	{
		if (actMode == eIS_Released)
		{
			NavigationAgentTypeID agentId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");

			if (pLandOwner->GetGroupMid())
			{
				SCitizenComponent *mid = pLandOwner->GetGroupMid()->GetEntity()->GetComponent<SCitizenComponent>();
				if (mid)
				{
					const Vec3 diff = mid->GetEntity()->GetWorldPos() - mouseTargetPos;

					mid->MoveToLocation(mouseTargetPos);

					for each(IUnit *unt in pLandOwner->GetGroupSelected())
					{
						if (SCitizenComponent *ctz = unt->GetEntity()->GetComponent<SCitizenComponent>())
						{
							if (ctz != mid)
								ctz->MoveToLocation(CorrectNavigationPoint(agentId, unt->GetEntity()->GetWorldPos() - diff));
						}
					}

				}
			}
		}
	}
}

void CPlayerComponent::CreateUnit(string unitName)
{
	if (pLandOwner)
	{
		IUnit *pUnit = pLandOwner->CreateUnit(unitName);
	}
}

void CPlayerComponent::CreateBuilding(string buildingName)
{
	if (pLandOwner)
		pLandOwner->CreateBuilding(buildingName);
}

void CPlayerComponent::Build()
{
	if (pLandOwner)
	{
		SBuildingComponent *pNewBuilding = pLandOwner->Build();
		if (pNewBuilding)
			SelectUnit(pNewBuilding);
	}
}

void CPlayerComponent::SelectUnit(IUnit * pUnit)
{
	if (pLandOwner)
	{
		if (pUnit && pUnit->GetOwner() != pLandOwner) 
			return;

		if (IUnit *previousUnit = pLandOwner->GetSelectedUnit())
			pHud->ShowUnitInfo(previousUnit->GetName(), false);

		pLandOwner->SelectUnit(pUnit);

		if (IUnit *newUnit = pLandOwner->GetSelectedUnit())
		{
			if (SBuildingComponent *pBuildingUnit = newUnit->GetEntity()->GetComponent<SBuildingComponent>())
			{
				if (pBuildingUnit->IsReady())
				{
					pHud->ShowUnitInfo(newUnit->GetName(), true);
				}
			}
			else
				pHud->ShowUnitInfo(newUnit->GetName(), true);
		}
	}
}

void CPlayerComponent::HandleInputFlagChange(TInputFlags flags, int activationMode, EInputFlagType type)
{
	switch (type)
	{
	case EInputFlagType::Hold:
	{
		if (activationMode == eIS_Released)
		{
			m_inputFlags &= ~flags;
		}
		else
		{
			m_inputFlags |= flags;
		}
	}
	break;
	case EInputFlagType::Toggle:
	{
		if (activationMode == eIS_Released)
		{
			m_inputFlags ^= flags;
		}
	}
	break;
	}
}

void CPlayerComponent::DebugDraw()
{
}

void CPlayerComponent::CreateHud()
{
	pHud = m_pEntity->GetOrCreateComponentClass<CHudComponent>();
	pHud->AddBuildingButton(1000, 870, "School", CGamePlugin::GetBuildCostByName("School"), CGamePlugin::GetBuildTimeByName("School"));
	pHud->AddBuildingButton(1000, 970, "Granary", CGamePlugin::GetBuildCostByName("Granary"), CGamePlugin::GetBuildTimeByName("Granary"));
	pHud->AddBuildingButton(1300, 870, "Gold_Mine", CGamePlugin::GetBuildCostByName("Gold_Mine"), CGamePlugin::GetBuildTimeByName("Gold_Mine"));
	pHud->AddBuildingButton(1300, 970, "Barracks", CGamePlugin::GetBuildCostByName("Barracks"), CGamePlugin::GetBuildTimeByName("Barracks"));

	pHud->AddUnitInfoPanel("School");
	pHud->SetVisibilityUnitInfo("School", false, true, true);
	pHud->AddButtonToInfoPanel("School", 10, 90, "Helper", CGamePlugin::GetBuildCostByName("Helper"), CGamePlugin::GetBuildTimeByName("Helper"));
	pHud->AddButtonToInfoPanel("School", 220, 90, "Worker", CGamePlugin::GetBuildCostByName("Worker"), CGamePlugin::GetBuildTimeByName("Worker"));
	pHud->ShowUnitInfo("School", false);

	pHud->AddUnitInfoPanel("Granary");
	pHud->SetVisibilityUnitInfo("Granary", false, true, false);
	pHud->ShowUnitInfo("Granary", false);

	pHud->AddUnitInfoPanel("Barracks");
	pHud->SetVisibilityUnitInfo("Barracks", false, true, true);
	pHud->AddButtonToInfoPanel("Barracks", 10, 90, "Warrior", CGamePlugin::GetBuildCostByName("Warrior"), CGamePlugin::GetBuildTimeByName("Warrior"));
	pHud->ShowUnitInfo("Barracks", false);

	pHud->AddUnitInfoPanel("Gold Mine");
	pHud->SetVisibilityUnitInfo("Gold Mine", true, true, false);
	pHud->ShowUnitInfo("Gold Mine", false);

	pHud->AddTextField("Gold", "Gold in all granaries: 0", 1500, 930, 30);

	pHud->ShowHud();
}

void CPlayerComponent::RotateBuilding(int direction)
{
	if (pLandOwner)
	{
		if (SBuildingComponent *pBuilding = pLandOwner->GetCreatingBuilding())
		{
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(pBuilding->GetEntity()->GetWorldRotation()));
			if (direction == 0)
			{
				ypr.x -= 0.1f;
			}
			else if (direction == 1)
			{
				ypr.x += 0.1f;
			}
			Quat newRotation = Quat(CCamera::CreateOrientationYPR(ypr));
			pBuilding->GetEntity()->SetRotation(newRotation);
		}
	}
}
