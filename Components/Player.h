#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <CryMath/Cry_Camera.h>

#include <ICryMannequin.h>

#include <DefaultComponents/Cameras/CameraComponent.h>
#include <DefaultComponents/Input/InputComponent.h>

struct SBuildingComponent;
class CLandOwnerComponent;
struct IUnit;
class CHudComponent;

class CPlayerComponent final : public IEntityComponent
{
	enum class EInputFlagType
	{
		Hold = 0,
		Toggle
	};

	typedef uint8 TInputFlags;

	enum class EInputFlag
		: TInputFlags
	{
		MoveLeft = 1 << 0,
		MoveRight = 1 << 1,
		MoveForward = 1 << 2,
		MoveBack = 1 << 3,
		MoveRotate = 1 << 4
	};

public:
	CPlayerComponent() = default;
	virtual ~CPlayerComponent() {}

	// IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CPlayerComponent>& desc);
	// ~IEntityComponent

	static void CVarHandler(IConsoleCmdArgs *pArgs);

	void Revive();
	void Action_CameraZoomIn();
	void Action_CameraZoomOut();
	void Action(int actMode);
	void WarriorAction(int actMode);

	void CreateUnit(string unitName);
	void CreateBuilding(string buildingName);
	void Build();
	void SelectUnit(IUnit *pUnit);
protected:
	void HandleInputFlagChange(TInputFlags flags, int activationMode, EInputFlagType type = EInputFlagType::Hold);
	void DebugDraw();
	void CreateHud();
	void RotateBuilding(int direction);

protected:
	Cry::DefaultComponents::CCameraComponent* m_pCameraComponent = nullptr;
	Cry::DefaultComponents::CInputComponent* m_pInputComponent = nullptr;
	CHudComponent *pHud = nullptr;
	TInputFlags m_inputFlags;
	Vec2 m_mouseDeltaRotation;
	const float rotationSpeed = 0.01f;
	Quat lookOrientation = IDENTITY;
	const float minCamYOffset = 0.f;
	const float minCamZOffset = 10.f;
	float camYOffset = minCamYOffset;
	float camZOffset = minCamZOffset;
	float zoomRatio = 1.f;
	const float maxZoom = 1.f;
	const float minZoom = 50.f;
	const float zoomStep = 0.2f;
	CLandOwnerComponent *pLandOwner = nullptr;
	IUnit *pTargetUnit = nullptr;
	Vec3 mouseTargetPos = ZERO;
	static string UNIT_CREATE;

	Vec3 hgRectStartPos = ZERO;
	Vec3 hgRectCurPos = ZERO;
	bool bHG = false;
};
