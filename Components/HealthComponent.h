/* -----------------------------------------------------------------------------------

Author : Michal Kolodziej
Project : AIFury Game Sample
Purpose : Health component applies health to entities and introduces death concept 

----------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

class CHealthComponent : public IEntityComponent
{
public:
	CHealthComponent() = default;
	CHealthComponent::~CHealthComponent() {}
	//IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CHealthComponent>& desc);
	//~IEntityComponent
	void Update(float frameTime);
	//Health
	void SetHealth(float health) { fValue = health; bIsAlive = health > 0.f; }
	void AddHealth(float health) { fValue += health; }
	void Damage(float damage);
	void SetMaxHealth(float max) { fMax = max; }
	void SetRegenerationRatio(float ratio) { fRegenerationRatio = ratio; }
	float GetHealth() { return fValue; }
	float GetMaxHealth() { return fMax; }
	float GetRegenerationRatio() { return fRegenerationRatio; }
	bool IsAlive() { return bIsAlive; }
	void Reset();
	//~Health
private:
	float fValue = 100.f;
	float fMax = 100.f;
	float fRegenerationRatio = 0.f;
	bool bIsAlive = false;
};