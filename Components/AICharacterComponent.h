#pragma once
#include <array>
#include <numeric>
#include <CryEntitySystem/IEntityComponent.h>
#include <CryMath/Cry_Camera.h>
#include <ICryMannequin.h>
#include "AICharacterProperties.h"
#include "EntityAudioComponent.h"

class CAICharacterComponent : public IEntityComponent
{
public:
	//Component system
	CAICharacterComponent() = default;
	virtual ~CAICharacterComponent() {}
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CAICharacterComponent>& desc);
	void Revive();
	void Spawn();
	void StartFire();
	void UpdateAnimation(float frameTime);
	void Update(float frameTime);
	//Use to calculate speed walk for AI
	void SetVelocity(Vec3 velocity);
	//Other AI stuff
	void GoTo(Vec3 pos);
	void Set_Timer(int timerId, int ms);
	void Kill_Timer(int timerId);
	bool Get_TimerState(int timerId);
	bool TimerExists(int timerId);
	void SetHasWeapon(bool has) { bHasWeapon = has; }
	//Getting components
	//Movement, animation components
	SCharacterControllerComponent *GetController() { return pController; }
	SAdvancedAnimationComponent *GetAnimations() { return pAnimations; }
	SPathfindingComponent *GetPathfinder() { return pPathfinder; }
	SAIFuryCharacterProperties GetProperties() { return ai_properties; }
	SEntityAudioComponent *GetAudioComponent() { return pAudio; }
	//
protected:
	//VARIABLES
	//Component pointers
	//Movement, animation components
	SCharacterControllerComponent *pController = nullptr;
	SAdvancedAnimationComponent *pAnimations = nullptr;
	SPathfindingComponent *pPathfinder = nullptr;
	SEntityAudioComponent *pAudio = nullptr;
	//
	//ANIMATIONS TECHNICAL
	TagID walkTagId;
	FragmentID m_activeFragmentId;
	TagID weaponTagId;
	bool bHasWeapon = false;
	//PROPERTIES
	SAIFuryCharacterProperties ai_properties;
	SAIFuryCharacterProperties ai_previousProperties;
public:
	//FOR BEHAVIOR TREE
	Vec3 lookDir = ZERO;
	bool bIsWaiting = false;
	int iWaitTime = 0;
	Vec3 lastVelocity = ZERO;
	Vec3 savedVelocity = ZERO;
	IEntity *entityShooting = nullptr;
	IEntity *noiseMakingEntity = nullptr;
	std::unordered_map<int, bool> timerSet;
};