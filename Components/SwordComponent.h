/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AIFury Strategy Sample
Purpose : Simple sword for damaging enemies

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

class CSwordComponent : public IEntityComponent
{
	enum TIMERS
	{
		Timer_Attack = 0
	};
public:
	CSwordComponent() = default;
	CSwordComponent::~CSwordComponent() {}
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CSwordComponent>& desc);
	// ~IEntityComponent
	virtual void Update(float frameTime);
	virtual void OnCollision(IEntity *pVictimEntity);
	//SWORD
	void StartAttack();
	void StopAttack();
	void SetOwner(IEntity *pOwner) { pOwnerEntity = pOwner; }
	IEntity *GetOwner() { return pOwnerEntity; }
	//~SWORD
protected:
	bool bIsAttacking = false;
	IEntity *pOwnerEntity = nullptr;
};
