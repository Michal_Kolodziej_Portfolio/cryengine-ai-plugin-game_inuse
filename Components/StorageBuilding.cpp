#include "StdAfx.h"
#include "StorageBuilding.h"
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "GlobalResources.h"

static void RegisterStorageBuildingComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CStorageBuilding));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterStorageBuildingComponent)

void CStorageBuilding::Initialize_Building()
{
	for (int i = 0; i < 10; i++)
	{
		storage.push_back(startValues);
	}
	bIsMine = false;
	bAutoProduction = false;
	bIsUnitCreator = false;
}

void CStorageBuilding::ReflectType(Schematyc::CTypeDesc<CStorageBuilding>& desc)
{
	desc.SetGUID("{41B49EE2-A3F9-436E-9F58-1FD251792F9A}"_cry_guid);
	desc.AddBase<IUnit>();
	desc.AddBase<SBuildingComponent>();
	desc.SetEditorCategory("Game");
	desc.SetLabel("Storage Building Component");
	desc.SetDescription("Handles storage buildings");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//editor properties
	desc.AddMember(&CStorageBuilding::bReadyByDefault, 'read', "ReadyByDefault", "Is ready by default", "Defines whether building should be fully builded from start", false);
	desc.AddMember(&CStorageBuilding::unitProperties, 'unit', "UnitProperties", "Unit settings", "Settings of this unit", SUnitProperties());
	desc.AddMember(&CStorageBuilding::geometryPath, 'geom', "Geometry", "Geometry", "Path to geometry file", "");
	desc.AddMember(&CStorageBuilding::materialPath, 'matp', "Material", "Material", "Path to material file", "");
	desc.AddMember(&CStorageBuilding::startValues, 'star', "StartValues", "Start values for storage", "Start values for storage building", 0);
}

std::vector<int> CStorageBuilding::GetStorage()
{
	return storage;
}

void CStorageBuilding::AddResource(int type, int ammount)
{
	storage[type] += ammount;
}

void CStorageBuilding::RemoveResource(int type, int ammount)
{
	storage[type] -= ammount;
}

void CStorageBuilding::SetResource(int type, int ammount)
{
	storage[type] = ammount;
}

int CStorageBuilding::GiveProducts(int giveAmmount, int type)
{
	AddResource(type, giveAmmount);
	return giveAmmount;
}

int CStorageBuilding::GetProductAmmount()
{
	return 0;
}

float CStorageBuilding::GetSupplyAmmount()
{
	return (float)storage[1];
}

int CStorageBuilding::GetProductType()
{
	return 0;
}

int CStorageBuilding::GetSupplyType()
{
	return 1;
}
