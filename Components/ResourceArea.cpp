#include "StdAfx.h"
#include "ResourceArea.h"
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "CryRenderer/IRenderer.h"
#include "GlobalResources.h"
#include "GamePlugin.h"

static void RegisterResourceAreaComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CResourceAreaComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterResourceAreaComponent)

void CResourceAreaComponent::Initialize()
{
	CGamePlugin::AddResourceArea(this);

	currentUnits = maxUnits;

	NavigationAgentTypeID agentId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
	mineSlotPos[0] = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + Vec3(10.f, 0.f, 0.f));
	mineSlotPos[1] = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + Vec3(-10.f, 0.f, 0.f));
}

uint64 CResourceAreaComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);
}

void CResourceAreaComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		Update(pCtx->fFrameTime);
	}
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
	}
	break;
	}
}

void CResourceAreaComponent::ReflectType(Schematyc::CTypeDesc<CResourceAreaComponent>& desc)
{
	desc.SetGUID("{CEA4B332-326D-4EA1-8882-569763BB9CCF}"_cry_guid);
	desc.SetEditorCategory("Game");
	desc.SetLabel("Resource Area Component");
	desc.SetDescription("Resource area can handle multiple desired resources");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//editor properties
	desc.AddMember(&CResourceAreaComponent::resourceType, 'rest', "ResourceType", "Resource type", "Name of product resource", EResourceTypes());
	desc.AddMember(&CResourceAreaComponent::maxUnits, 'maxu', "MaxUnits", "Max Resource Units", "Maximum resource units that this area can handle", 10000.f);
	desc.AddMember(&CResourceAreaComponent::areaRadius, 'arad', "AreaRadius", "Area Radius", "Radius within which this area can be exploated", 5.f);
	desc.AddMember(&CResourceAreaComponent::areaColor, 'arec', "AreaColor", "Area Color", "Color of this area", ColorF());
}

void CResourceAreaComponent::Update(float frameTime)
{
	if (gEnv->IsEditing())
	{ 
		Vec3 vec[24];
		DrawCircleOnTerrain(m_pEntity->GetWorldPos(), areaRadius, areaColor, 10.f, vec);
	}
}

bool CResourceAreaComponent::Utilize(float ammount)
{
	if (currentUnits >= ammount)
	{
		currentUnits -= ammount;
		return true;
	}
	return false;
}

void CResourceAreaComponent::AddMine(SBuildingComponent * pMine)
{
	if (!pMine)
		return;

	for (int i = 0; i < 2; i++)
	{
		if (!pMines[i])
		{
			pMines[i] = pMine;
			return;
		}
	}
}
