/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AIFury Strategy Sample
Purpose : Base class for buildings and citizens for selection handling

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

class CHealthComponent;
class CLandOwnerComponent;

struct IUnit : public IEntityComponent
{
protected:
	struct SUnitProperties
	{
		inline bool operator==(const SUnitProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
		inline bool operator!=(const SUnitProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

		int buildCost = 0;
		int buildTime = 0;
		int team = 0;

		static void ReflectType(Schematyc::CTypeDesc<SUnitProperties>& desc)
		{
			desc.SetGUID("{F935A470-C4BE-47CD-9754-05FF9BBE4DC7}"_cry_guid);
			desc.AddMember(&SUnitProperties::buildCost, 'bcos', "BuildCost", "Build gold cost", "How much gold will this unit take to build it", 0);
			desc.AddMember(&SUnitProperties::buildTime, 'btim', "BuildTime", "Build time", "How much time will this unit take to build it", 0);
			desc.AddMember(&SUnitProperties::team, 'utim', "Team", "Team", "Which team this unit should belong to. Teams are numerated simply", 0);
		}
	};
public:
	IUnit() = default;
	IUnit::~IUnit() {}
	static void ReflectType(Schematyc::CTypeDesc<IUnit>& desc);
	// IEntityComponent
private:
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	// ~IEntityComponent
	virtual void Update(float frameTime);
protected:
	virtual uint64 GetEventMask_Unit() const { return 0; }
	virtual void ProcessEvent_Unit(SEntityEvent& event) {}
	virtual void Update_Unit(float frameTime) {}
	virtual void Timer_Unit(int timerId) {}
public:
	//Unit
	virtual void Select();
	virtual void Deselect();
	virtual void ShowSelectedInfo();
	virtual void SetOwner(CLandOwnerComponent *pNewOwner) { pLandOwner = pNewOwner; }
	virtual CLandOwnerComponent *GetOwner() { return pLandOwner; }
	virtual bool IsOwner(CLandOwnerComponent *LandOwnerToCheck) { return LandOwnerToCheck == pLandOwner; }
	virtual int GetCost() { return unitProperties.buildCost; }
	virtual int GetTime() { return unitProperties.buildTime; }
	virtual int GetTeam() { return unitProperties.team; }
	virtual void SetTeam(int tm) { unitProperties.team = tm; }
	virtual void SetTeam();
	virtual void GetCirclePoints(Vec3 *vec) { for (int i = 0; i < 24; i++) { vec[i] = circlePts[i]; } }
	virtual bool IsSelected() { return bIsSelected; }
	//~Unit
protected:
	bool bIsInitialized = false;
	bool bIsSelected = false;
	CHealthComponent *pHealth = nullptr;
	CLandOwnerComponent *pLandOwner = nullptr;
	SUnitProperties unitProperties;
	float highlightRadius = 0.f;
	ColorF highlightCircleColor = Col_Green;
	Vec3 circlePts[24];
};
