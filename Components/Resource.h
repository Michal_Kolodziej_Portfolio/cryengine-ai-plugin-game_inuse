#pragma once
#include <CryEntitySystem/IEntityComponent.h>

struct SResource : public IEntityComponent
{
	SResource() = default;
	SResource::~SResource() {}
	// IEntityComponent
	static void ReflectType(Schematyc::CTypeDesc<SResource>& desc);
	// ~IEntityComponent
};