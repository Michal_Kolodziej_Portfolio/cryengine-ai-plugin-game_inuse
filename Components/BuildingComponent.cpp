#include "StdAfx.h"
#include "BuildingComponent.h"
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "CryRenderer/IRenderer.h"
#include "GlobalResources.h"
#include "HealthComponent.h"
#include "ResourceArea.h"
#include "CitizenComponent.h"
#include "StorageBuilding.h"

static void RegisterBuildingComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(SBuildingComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterBuildingComponent)

void SBuildingComponent::Initialize()
{
	highlightRadius = 10.f;
	highlightCircleColor = Col_Green;
	LoadGeometry();
	Ghost(true);
	Physicalize();

	Initialize_Building();
}

void SBuildingComponent::ReflectType(Schematyc::CTypeDesc<SBuildingComponent>& desc)
{
	desc.SetGUID("{072313C5-3DAF-4ED4-8AEB-5481F149C3BA}"_cry_guid);
	desc.AddBase<IUnit>();
	desc.SetEditorCategory("Game");
	desc.SetLabel("Building Component");
	desc.SetDescription("Handles various buildings");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//editor properties
	desc.AddMember(&SBuildingComponent::unitProperties, 'unit', "UnitProperties", "Unit settings", "Settings of this unit", SUnitProperties());
	desc.AddMember(&SBuildingComponent::geometryPath, 'geom', "Geometry", "Geometry", "Path to geometry file", "");
	desc.AddMember(&SBuildingComponent::materialPath, 'matp', "Material", "Material", "Path to material file", "");
	//editor resource management
	desc.AddMember(&SBuildingComponent::bReadyByDefault, 'read', "ReadyByDefault", "Is ready by default", "Defines whether building should be fully builded from start", false);
	desc.AddMember(&SBuildingComponent::bIsMine, 'ismi', "IsMine", "Is building a mine", "Defines whether a building is mine or normal building", false);
	desc.AddMember(&SBuildingComponent::bIsUnitCreator, 'isuc', "IsUnitCreator", "Is building other units", "Defines whether the building is able to create other units", false);
	desc.AddMember(&SBuildingComponent::bAutoProduction, 'autp', "AutoProduction", "Auto production", "Should building auto produce, or be triggered to produce", false);
	desc.AddMember(&SBuildingComponent::productType, 'resp', "ResourceProducing", "Product type", "Name of product resource", EResourceTypes());
	desc.AddMember(&SBuildingComponent::supplyType, 'rest', "ResourceStoring", "Supply type", "Name of product taken in to produce product", EResourceTypes());
	desc.AddMember(&SBuildingComponent::maxProductStored, 'pros', "MaxProductStored", "Max product storage", "Max ammount of products stored", 0);
	desc.AddMember(&SBuildingComponent::maxTakeStored, 'prox', "MaxTakeResourceStored", "Max take resource storage", "Max ammount of taken resources stored", 0.f);
	desc.AddMember(&SBuildingComponent::productionTick, 'prot', "ProductionTick", "Time to produce single product", "Time to produce single product", 0);
	desc.AddMember(&SBuildingComponent::supplyPerProduct, 'supr', "SupplyPerProduct", "Supply ammount to produce single product", "How many supplies are needed to produce one product", 0.f);
	desc.AddMember(&SBuildingComponent::resourceMinedAtOnce, 'reot', "MiningRatio", "Mining ratio", "How much of resource is taken at one tick. Mine only.", 0.f);
}

void SBuildingComponent::Update_Unit(float frameTime)
{
	if (bIsReady && bIsMine)
	{
		//if (GetTeam() == 0)
		//{
		//	if (reservedSuppliesTaker)
		//		CryLogAlways("%s / %s / %d HAS SUPPLIER TAKER %s / %d", m_pEntity->GetName(), GetName(), m_pEntity->GetId(), reservedSuppliesTaker->GetName(), reservedSuppliesTaker->GetId());
		//	else
		//		CryLogAlways("%s / %s / %d DOES NOT HAVE SUPPLIER TAKER", m_pEntity->GetName(), GetName(), m_pEntity->GetId());
		//}
	}
	if (!bIsSettled)
	{
		AABB bounds;
		m_pEntity->GetWorldBounds(bounds);
		const Vec3 size = bounds.GetSize();
		NavigationAgentTypeID agentId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
		supplyPosition = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + (m_pEntity->GetForwardDir() * (5.f + (size.y / 2.f))) + Vec3(-1.5f, 0.f, 0.f));
		depositPosition = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + ((m_pEntity->GetForwardDir() * (5.f + (size.y / 2.f))) + Vec3(1.5f, 0.f, 0.f)));
		unitSpawnPosition = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + ((m_pEntity->GetForwardDir() * (5.f + (size.y / 2.f))) + Vec3(3.f, 0.f, 0.f)));
		highlightRadius = (size.x > size.y) ? size.x : size.y;
	}

	if (!pHealth)
	{
		pHealth = m_pEntity->GetComponent<CHealthComponent>();
		if (pHealth && !bIsInitialized)
		{
			bIsInitialized = true;
			if (bReadyByDefault)
			{
				BuildFully();
			}
			else
				pHealth->SetHealth(0.f);
		}
	}

	if(bAutoProduction)
		Produce();
	else if (bIsUnitCreator)
	{
		if (pProducedCitizen)
		{
			ProduceCitizen();
		}
	}

	if(!IsReady())
	{ 
		bIsInvalidPlacement = !CanBuildHere();

		if (bLastPlacementValidity != bIsInvalidPlacement)
		{
			bLastPlacementValidity = bIsInvalidPlacement;
			Ghost(!bIsInvalidPlacement);
		}
	}
}

void SBuildingComponent::Timer_Unit(int timerId)
{
	if (timerId == Timer_Production)
	{
		ProduceResource();
		bIsProducing = false;
	}
	if (timerId == Timer_Mine)
	{
		bIsProducing = false;
		if (currentTakeResources < maxTakeStored)
		{
			currentTakeResources += resourceMinedAtOnce;
			if (currentTakeResources >= supplyPerProduct)
			{
				currentTakeResources -= supplyPerProduct;
				ProduceResource();
			}
		}
	}
	if (timerId == Timer_Citizen_Production_Tick)
	{
		bIsProducing = false;
		if (pProducedCitizen)
		{
			citizenProductionTimeLeft -= 1;

			if (citizenProductionTimeLeft <= 0)
			{
				citizenProductionTimeLeft = 0;
				string citizenType = pProducedCitizen->GetName();
				citizenType = citizenType.MakeLower();
				const string className = "schematyc::schematycentities::" + citizenType;
				pLandOwner->RemoveUnit(pProducedCitizen);
				pProducedCitizen->GetEntity()->RemoveAllComponents();
				gEnv->pEntitySystem->RemoveEntity(pProducedCitizen->GetEntityId(), true);
				pProducedCitizen = nullptr;
				IEntityClass *pEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(className);
				if (pEntityClass)
				{
					SEntitySpawnParams spawn;
					spawn.pClass = pEntityClass;
					if (IEntity *pUnitEntity = gEnv->pEntitySystem->SpawnEntity(spawn))
					{
						if (SCitizenComponent *pCitizen = pUnitEntity->GetComponent<SCitizenComponent>())
						{
							pCitizen->GetEntity()->SetPos(unitSpawnPosition);
							pCitizen->SetOwner(pLandOwner);
							pLandOwner->AddUnit(pCitizen);
							pCitizen->SetTeam(pLandOwner->GetTeam());
						}
					}
				}
			}
		}
	}
}

void SBuildingComponent::ProcessEvent_Unit(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (bReadyByDefault_Last != bReadyByDefault)
		{
			bReadyByDefault_Last = bReadyByDefault;
			if (bReadyByDefault)
				BuildFully();
			else
			{
				Ghost(true);
				if (pHealth)
					pHealth->SetHealth(0.f);
			}
		}
	}
	break;
	}
}

void SBuildingComponent::Produce()
{
	if (bIsReady && !bIsProducing && currentProducts < maxProductStored)
	{
		if (bIsMine)
		{
			if (pResourceArea)
			{
				if (pResourceArea->Utilize(resourceMinedAtOnce))
				{
					m_pEntity->SetTimer(Timer_Mine, productionTick);
					bIsProducing = true;
				}
			}
		}
		else if(currentTakeResources >= supplyPerProduct)
		{
			currentTakeResources -= supplyPerProduct;
			m_pEntity->SetTimer(Timer_Production, productionTick);
			bIsProducing = true;
		}
	}
}

void SBuildingComponent::ProduceCitizen()
{
	if (bIsReady && !bIsProducing)
	{
		if (bIsUnitCreator)
		{
			if (pProducedCitizen)
			{
				m_pEntity->SetTimer(Timer_Citizen_Production_Tick, 1000);
				bIsProducing = true;
			}
		}
	}
}

bool SBuildingComponent::CanBuildHere()
{
	if (!bIsSettled)
	{
		NavigationAgentTypeID agentId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
		if (gEnv->pAISystem->GetNavigationSystem()->IsLocationValidInNavigationMesh(agentId, supplyPosition))
		{
			if (gEnv->pAISystem->GetNavigationSystem()->IsLocationValidInNavigationMesh(agentId, depositPosition))
			{
				if (gEnv->pAISystem->GetNavigationSystem()->IsLocationValidInNavigationMesh(agentId, unitSpawnPosition))
				{
					DrawCircleOnTerrain(m_pEntity->GetWorldPos(), highlightRadius, highlightCircleColor, 10.f, circlePts, true, agentId);
					int bl = 0;
					for (int i = 0; i < 24; i++)
					{
						if (gEnv->pAISystem->GetNavigationSystem()->IsLocationValidInNavigationMesh(agentId, circlePts[i]))
						{
							bl++;
						}
					}
					if (bl == 24)
					{
						if (bIsMine)
						{
							for each(CResourceAreaComponent *res in CGamePlugin::GetResourceAreas())
							{
								if (res->GetResourceType() == GetSupplyType())
								{
									const float dist = m_pEntity->GetWorldPos().GetDistance(res->GetEntity()->GetWorldPos());
									if (dist <= res->GetAreaRadius())
									{
										if (res->HasSpaceForMine())
										{
											pResourceArea = res;
											return true;
										}
									}
								}
							}
							return false;
						}
						else return true;
					}
				}
			}
		}
		return false;
	}
	return true;
}

void SBuildingComponent::ProgressWork()
{
	buildProgress++;

	if (pHealth)
	{
		float maxHealth = pHealth->GetMaxHealth();
		float healthBuildStep = maxHealth / unitProperties.buildTime;
		pHealth->AddHealth(healthBuildStep);
	}

	if (buildProgress == unitProperties.buildTime)
	{
		BuildFully();
		buildProgress = 0;
	}
}

bool SBuildingComponent::Build()
{
	AABB bounds;
	m_pEntity->GetWorldBounds(bounds);
	const Vec3 size = bounds.GetSize();
	NavigationAgentTypeID agentId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
	supplyPosition = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + (m_pEntity->GetForwardDir() * (5.f + (size.y / 2.f))) + Vec3(-1.5f, 0.f, 0.f));
	depositPosition = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + ((m_pEntity->GetForwardDir() * (5.f + (size.y / 2.f))) + Vec3(1.5f, 0.f, 0.f)));
	unitSpawnPosition = CorrectNavigationPoint(agentId, m_pEntity->GetWorldPos() + ((m_pEntity->GetForwardDir() * (5.f + (size.y / 2.f))) + Vec3(3.f, 0.f, 0.f)));
	highlightRadius = (size.x > size.y) ? size.x : size.y;

	if (CanBuildHere())
	{
		if (pHealth)
			pHealth->SetHealth(0.f);

		if (pLandOwner)
			pLandOwner->BearTheCosts(unitProperties.buildCost);

		ExcludeFromNavigation();

		if (pResourceArea && bIsMine)
			pResourceArea->AddMine(this);

		bIsSettled = true;

		return true;
	}
	return false;
}

void SBuildingComponent::LoadGeometry()
{
	m_pEntity->FreeSlot(0);
	m_pEntity->LoadGeometry(0, geometryPath.value.c_str());
}

void SBuildingComponent::Ghost(bool valid)
{
	IMaterial *pGhostMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("Materials/building_ghost");
	if(!valid)
		pGhostMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("Materials/building_ghost_invalid");

	if (pGhostMaterial)
		m_pEntity->SetMaterial(pGhostMaterial);
}

void SBuildingComponent::Materialize()
{
	IMaterial *pMat = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(materialPath.value.c_str());
	if (pMat)
		m_pEntity->SetMaterial(pMat);
}

void SBuildingComponent::SetupProxy()
{
	IMaterial *pMat = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("Materials/proxy_no_draw");
	if (pMat)
	{
		IStatObj *pProxy = m_pEntity->GetStatObj(1);
		if (pProxy)
		{
			pProxy->SetMaterial(pMat);
		}
	}
}

void SBuildingComponent::Physicalize()
{
	SEntityPhysicalizeParams phys;
	phys.type = PE_STATIC;
	m_pEntity->Physicalize(phys);
}

void SBuildingComponent::ProduceResource()
{
	if (currentProducts < maxProductStored)
	{
		currentProducts++;
	}
}

void SBuildingComponent::SupplyResource()
{
	if (currentTakeResources < maxTakeStored)
	{
		currentTakeResources++;
	}
}

int SBuildingComponent::GrabProducts(int grabAmmount, int type)
{
	currentProducts -= grabAmmount;

	if (currentProducts >= 0)
		return grabAmmount;
	
	int prd = grabAmmount + currentProducts;
	currentProducts = 0;
	return prd;
}

int SBuildingComponent::GiveProducts(int giveAmmount, int type)
{
	float newAmmount = currentTakeResources + giveAmmount;

	if (newAmmount > maxTakeStored)
	{
		currentTakeResources = maxTakeStored;
		return (int)(giveAmmount - (newAmmount - maxTakeStored));
	}

	currentTakeResources += giveAmmount;
	return giveAmmount;

}

IUnit * SBuildingComponent::CreateUnit(string unitName)
{
	if (pProducedCitizen || currentTakeResources < 30.f)
		return nullptr;

	unitName = unitName.MakeLower();
	const string className = "schematyc::schematycentities::" + unitName;
	IEntityClass *pEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(className);
	if (pEntityClass)
	{
		SEntitySpawnParams spawn;
		spawn.pClass = pEntityClass;
		if (IEntity *pUnitEntity = gEnv->pEntitySystem->SpawnEntity(spawn))
		{
			DynArray<IEntityComponent*> cmps;
			pUnitEntity->GetComponents(cmps);
			const string cmpToRemove = "AI Fury Component";
			for (int i = 0; i < pUnitEntity->GetComponentsCount(); i++)
			{
				if (cmps[i])
				{
					if (cmps[i]->GetName() == cmpToRemove)
					{
						pUnitEntity->RemoveComponent(cmps[i]);
					}
				}
			}
			if (SCitizenComponent *pCitizen = pUnitEntity->GetComponent<SCitizenComponent>())
			{
				if (pCitizen->GetCost() <= currentTakeResources)
				{
					currentTakeResources -= pCitizen->GetCost();
					pProducedCitizen = pCitizen;
					citizenProductionTimeLeft = pCitizen->GetTime();
					pCitizen->SetOwner(pLandOwner);
					pCitizen->SetTeam(pLandOwner->GetTeam());
					pLandOwner->AddUnit(pCitizen);
					return pCitizen;
				}
			}
		}
	}
	return nullptr;
}

Vec3 SBuildingComponent::OccupyBuildPoint()
{
	for (int i = 0; i < 24; i++)
	{
		if (!buildOccupancy[i])
		{
			buildOccupancy[i] = true;
			return circlePts[i];
		}
	}
	return ZERO;
}

Vec3 SBuildingComponent::GetValidPositionInRadiusForBuilding(const Vec3 origin, float radius)
{
	Vec3 correctedByNavigation;
	NavigationAgentTypeID agentId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");
	gEnv->pAISystem->GetNavigationSystem()->GetClosestPointInNavigationMesh(agentId, origin, 100.f, 100.f, &correctedByNavigation);

	Vec3 newOrigin = correctedByNavigation;
	int step = (int)(radius / 0.1f);
	Vec3 topOrg = newOrigin + Vec3(0.f, -radius, 0.f);
	//top right
	for (int i = 0; i < step; i++)
	{
		topOrg.y += 0.1f;
		for (int x = 0; x < step; x++)
		{
			topOrg.x += 0.1f;

			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(topOrg, 1.f, Col_Red);

			m_pEntity->SetPos(topOrg);
			if (CanBuildHere())
			{
				CryLogAlways("CAN BUILD AT %f %f %f", topOrg.x, topOrg.y, topOrg.z);
				return topOrg;
			}
		}
	}
	topOrg = newOrigin + Vec3(0.f, -radius, 0.f);
	//top left
	for (int i = 0; i < step; i++)
	{
		topOrg.y += 0.1f;
		for (int x = 0; x < step; x++)
		{
			topOrg.x -= 0.1f;

			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(topOrg, 1.f, Col_Red);

			m_pEntity->SetPos(topOrg);
			if (CanBuildHere())
			{
				CryLogAlways("CAN BUILD AT %f %f %f", topOrg.x, topOrg.y, topOrg.z);
				return topOrg;
			}
		}
	}
	return ZERO;
}

void SBuildingComponent::BuildFully()
{
	LoadGeometry();
	Materialize();
	Physicalize();
	if (pHealth)
		pHealth->Reset();

	bIsReady = true;
}

void SBuildingComponent::ExcludeFromNavigation()
{
	AABB bounds;
	m_pEntity->GetWorldBounds(bounds);

	const Vec3 size = bounds.GetSize();
	const Vec3 origin = m_pEntity->GetWorldPos() - Vec3(size.x / 2.f, size.y / 2.f, 0.f);

	Vec3 meshVertices[4];
	meshVertices[0] = origin;
	meshVertices[1] = meshVertices[0] + Vec3(size.x, 0.f, 0.f);
	meshVertices[2] = meshVertices[1] + Vec3(0.f, size.y, 0.f);
	meshVertices[3] = meshVertices[2] + Vec3(-size.x, 0.f, 0.f);


	char bf[32];
	const string id_t_str = itoa((int)m_pEntity->GetId(), bf, 10);
	const string meshName = "BUILDING_NAVMESH" + id_t_str;
	navMeshID = CreateNavigationMesh(meshVertices, 4, 5.f, Vec3(10.f, 10.f, 10.f), 100, "MediumSizedCharacters", meshName, true);
}
